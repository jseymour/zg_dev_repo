﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.3.3.196?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="time" Source="time.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="Agway191" Source="Valve.Agway191.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Agway192" Source="Valve.Agway192.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Agway193" Source="Valve.Agway193.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="FuelVlv" Source="Valve.FuelVlv.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank201" Source="Tanks.Tank201.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank202" Source="Tanks.Tank202.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank203" Source="Tanks.Tank203.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank204" Source="Tanks.Tank204.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank205" Source="Tanks.Tank205.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank206" Source="Tanks.Tank206.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank207" Source="Tanks.Tank207.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank208" Source="Tanks.Tank208.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank209" Source="Tanks.Tank209.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank210" Source="Tanks.Tank210.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank211" Source="Tanks.Tank211.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank212" Source="Tanks.Tank212.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank214" Source="Tanks.Tank214.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank216" Source="Tanks.Tank216.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank218" Source="Tanks.Tank218.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank220" Source="Tanks.Tank220.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank222" Source="Tanks.Tank222.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank224" Source="Tanks.Tank224.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Tank226" Source="Tanks.Tank226.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3">
    <Task Name="mbm_plc" Source="Communication.mbm_plc.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="mbs_rca" Source="Communication.mbs_rca.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="io_monitor" Source="io_monitor.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Alarm_NoOf" Source="Alarm_NoOffset.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Alarms" Source="HMI.Alarms.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="io_diag" Source="Sources.Service.Diagnostics.io_diag.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8">
    <Task Name="visuals" Source="HMI.visuals.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="plclog" Source="HMI.plclog.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <DataObjects>
    <DataObject Name="version" Source="version.dob" Memory="UserROM" Language="Simple" />
    <DataObject Name="dm_plc" Source="Communication.dm_plc.dob" Memory="UserROM" Language="Simple" />
  </DataObjects>
  <VcDataObjects>
    <VcDataObject Name="Visu" Source="HMI.Visu.dob" Memory="UserROM" Language="Vc" WarningLevel="2" Compress="false" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arial" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpwd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialbd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="verabd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfar00" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpkat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vera" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchtml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcxml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccurl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccgauge" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcclbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="operator" Source="Libraries.operator.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Convert" Source="Libraries.Convert.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DRV_mbus" Source="Libraries.DRV_mbus.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="dvframe" Source="Libraries.dvframe.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DRV_mn" Source="Libraries.DRV_mn.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="bpl_lib" Source="Libraries.bpl_lib.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="uMB" Source="Libraries.uMB.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="com_lib" Source="Libraries.com_lib.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="uSftMask" Source="Libraries.uSftMask.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.AsUSB.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsArSdm" Source="Libraries.AsArSdm.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="visapi" Source="Libraries.visapi.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="ModDiag" Source="Sources.Service.Diagnostics.ModDiag.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="AsIO" Source="Libraries.AsIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIODiag" Source="Libraries.AsIODiag.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIOMMan" Source="Libraries.AsIOMMan.lby" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>