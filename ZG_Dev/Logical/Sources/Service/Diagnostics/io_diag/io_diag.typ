(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: io_diag
 * File: io_diag.typ
 * Author: rainert
 * Created: June 23, 2010
 ********************************************************************
 * Local data types of program io_diag
 ********************************************************************)

TYPE
	ModuleInformation_typ : 	STRUCT 
		ModulePath : STRING[30]; (*module path*)
		ModuleName : STRING[20]; (*module name*)
		ModuleState : USINT; (*module state*)
		ModuleColor : USINT; (*module color*)
		ErrorModuleName : STRING[20]; (*name of module with error*)
	END_STRUCT;
	hmi_IO_info : 	STRUCT 
		ModuleName : STRING[20]; (*module name*)
		ModulePath : STRING[30]; (*module path*)
		ModuleStatus : USINT; (*module status*)
		PVName : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF STRING[32]; (*PV names*)
		LogicalName : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF STRING[40]; (*logical names*)
		Type : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF USINT; (*data type*)
		ActValue : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF LREAL; (*actual value*)
		ForceValue : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF LREAL; (*force value*)
		MinValue : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF LREAL; (*minimum value*)
		MaxValue : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF LREAL; (*maximum value*)
		Color : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF UINT; (*color of each row*)
		NumericColor : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF UINT; (*color for numeric fields*)
		EnableForcing : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF BOOL; (*enable forcing*)
		InivisibleLines : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF USINT; (*hide rows datapoint*)
		CmdBack : BOOL; (*back to module list*)
		CmdReset : BOOL; (*switch forcing off for all IOs*)
		CmdForcing : BOOL; (*force IO*)
		CompletionCmdForcing : BOOL; (*completion datapoint for forcing button*)
		ForcingCompletion : ARRAY[0..MAX_NUMBER_OF_IO_LINES] OF BOOL; (*forcing completion for each row*)
		ForceActive : ARRAY[0..MAX_NUMBER_OF_IO_LINES]OF BOOL; (*forcing active for each row*)
		CmdLineUp : BOOL; (*cursor line up*)
		CmdLineDown : BOOL; (*cursor line down*)
		CmdEndList : BOOL; (*cursor to very end*)
		CmdStartList : BOOL; (*cursor to very top*)
	END_STRUCT;
	hmi_module_info : 	STRUCT 
		ModuleName : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF STRING[20]; (*module name*)
		ModulePath : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF STRING[30]; (*module path*)
		ModuleState : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF USINT; (*module state*)
		ErrorModuleName : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF STRING[20]; (*error module name*)
		Color : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF UINT; (*color of each row*)
		WrongModulStatus : ARRAY[0..MAX_NUMBER_MODULE_LINES] OF USINT; (*wrong module inserted status*)
		CmdLineUp : BOOL; (*cursor line up*)
		CmdLineDown : BOOL; (*cursor line down*)
		CmdPageUp : BOOL; (*cursor page up*)
		CmdPageDown : BOOL; (*cursor page down*)
		CmdEndList : BOOL; (*cursor to very end*)
		CmdStartList : BOOL; (*cursor to very top*)
	END_STRUCT;
	hmi_io_diag : 	STRUCT 
		CmdDetails : BOOL; (*display configured I/Os of module*)
		ModuleInfo : hmi_module_info; (*module information*)
		IOInfo : hmi_IO_info; (*I/O information*)
		CmdExit : BOOL; (*exit the I/O view*)
		ModuleInfoLayerStatus : USINT; (*runitme datapoint of module information layer*)
		IOInfoLayerStatus : USINT; (*runtime datapoint of I/O info layer*)
		StatusLayerMessage : USINT; (*runtime datapoint of warning message layer*)
	END_STRUCT;
END_TYPE
