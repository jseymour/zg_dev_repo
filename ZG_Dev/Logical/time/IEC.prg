﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.3.3.196?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Cyclic code">timeCyclic.ab</File>
    <File Description="Initialization code">timeInit.ab</File>
    <File Description="Local data types" Private="true">time.typ</File>
    <File Description="Local variables" Private="true">time.var</File>
  </Files>
</Program>