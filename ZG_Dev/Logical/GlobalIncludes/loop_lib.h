/********************************************************************************/
/*                                                                              */
/*  loop_lib.h                                                                  */
/*  library for creating hybrid libraries, that works with both PG2000 and      */
/*  B&R Automation Studio                                                       */
/*      02.12.1998                                                              */
/*                                                                              */
/*      Automation Studio                                                       */
/*  Copyright Bernecker&Rainer 1998                                             */
/*                                                                              */
/********************************************************************************/

/*avoid multible includes*/
#ifndef __LOOP_LIB_H__
#define __LOOP_LIB_H__

/* Structure definition for function block call out of Automation Studio/C */
/* variables have to be sorted fist the non boolean and then the boolean   */
/* INPUTS have to be before the OUTPUTS and last comes internal variables  */
/* Define boolean variables as unsigned char, because ANSI-C knows no type */
/* for boolean expressions, the type plcbit does not work in structures.   */

typedef struct pid_para_typ {
    unsigned long    Ident;
    short int   Kp;
    short int   Kw;
    long  int   Ki;
    short int   Kb;
    short int   Kv;
    short int   Kf;
    short int   y_max;
    short int   y_min;
    short int   delta_y;
    short int   xw_max;
    short int   xw_min;
    short int   Td;
    unsigned char    fbk_mode;
    unsigned char    d_chng;
    long int   Ipart;
    long int   Ipart2;
    long int   Dpart;
    long int   P;
    long int   a1;
    long int   a2n;
    short int   youtold;
    long int   R;
    short int   time_count;
    } pid_para_typ;


/********************************************************************************/
/* 1.) pid                                                                      */
/*                                                                              */
/* description:  PID Controller                                                 */
/*                                                                              */
/* Attention: the controller parameters come from pid_para function block       */
/********************************************************************************/
/* Structure definition for function block call out of Automation Studio/C */
typedef struct {
	/* BYTE Input  Parameter */
	short int   W;						/*Set value*/
	short int   X;						/*Actual value*/
    short int   A;						/*Additional value*/
    short int   Y_man;					/*Controller output for manual mode*/
    short int   Y_fbk;					/*Returned correction value*/
    unsigned char    out_mode;			/*Operation mode*/
    struct pid_para_typ    *para_ptr;	/*Pointer for controller parameter structure*/
/*	unsigned long para_ptr;*/
	/* BYTE Output Parameter */
    unsigned short    status;			/*Controller status*/
	short int   Y;						/*Controller output*/
    short int   I_;						/*Integral value*/
    short int   D_;						/*Differential value*/
    /* BYTE Static Local     */
	/* BIT  Input  Parameter */
	unsigned char enable;				/*Enable*/
    unsigned char reset;				/*Reset for Integral value and Differential value*/
    /* BIT  Output Parameter */
    unsigned char E_pos;				/*Positive controller deviation alarm*/
    unsigned char E_neg;				/*Negative controller deviation alarm*/
    /* BIT  Internal Parameter */
}pid_Typ;
/* prototype for function block call out of Automation Studio C */
void PID (pid_Typ* param);


/********************************************************************************/
/* 2.) pid_min                                                                  */
/*                                                                              */
/* description:  PID Controller minimal version                                 */
/*                                                                              */
/* Attention: the controller parameters come from pid_para function block       */
/********************************************************************************/
/* Structure definition for function block call out of Automation Studio/C */
typedef struct  {
    /* BYTE Input  Parameter */
    short int   W;						/*Set value*/
    short int   X;						/*Actual value*/
    struct pid_para_typ    *para_ptr;	/*Pointer to parameter structure*/
    /* BYTE Output Parameter */
    unsigned short    status;			/*Controller status*/
    short int   Y;						/*Controller output*/
    short int   I_;						/*Integral value*/
    short int   D_;						/*Differential value*/
    /* BYTE Static Local     */
    /* BIT  Input  Parameter */
    unsigned char enable;				/*Enable*/
    unsigned char reset;				/*Reset for internal integrator value and Differential value*/
    /* BIT  Output Parameter */
    /* BIT  Internal Parameter */
} pid_min_Typ;
/* prototype for function block call out of Automation Studio C */
void PID_MIN (pid_min_Typ* param);

/********************************************************************************/
/* 3.) pid_para                                                                 */
/*                                                                              */
/* description:  PID Parametrization function                                   */
/*				 this function block enables the user to give in all parameters */
/*				 as physical values. It calculates the internal parameters from */
/*				 the physical controller parameters, corresponding to the chosen*/
/*				 sample time													*/
/*                                                                              */
/* Only in this function block the controller parameters may be entered.        */
/********************************************************************************/
/* Structure definition for function block call out of Automation Studio/C */
typedef struct {
        /* BYTE Input  Parameter */
        float   Ta;						/*Scan time [0.004 ...127.9 s]*/
        float   Y_max;					/*Upper correction value limit [0...99.9%]*/
        float   Y_min;					/*Lower correction value limit [0...99.9%]*/
        float   dY_max;					/*Maximum correction value ramp [0...100/Ta %]*/
        float   Kp;						/*Proportional gain [0...127.9]*/
        float   Tn;						/*Integration time [4xTa...32767xTa]*/
        float   Tv;						/*Derivative time [0...32767xTa]*/
        float   Tf;						/*Filter time [0, Tv/127 ... Tv/2]*/
        float   Kw;						/*Set value damping [0...2]*/
        float   Kr;						/*Windup damping [0...10], 0 is not useful*/
        float   e_pos;					/*Positive controller deviation tolerance [0...99.9]*/
        float   e_neg;					/*Negative controller deviation tolerance [0...-99.9]*/
        float   Td;						/*Alarm delay [Ta...32767*Ta]*/
        unsigned char    fbk_mode;		/*Feedback mode for correction value: 0=internal, 1=external*/
        unsigned char    d_mode;		/*Difference mode: 0=only X, 1= (W - X)*/
        struct  pid_para_typ    *para_ptr; /*Pointer to parameter structure*/
        /* BYTE Output Parameter */
        unsigned short    status;		/*Controller status*/
        /* BYTE static local */
        /* BIT  Input  Parameter */
        unsigned char enable;			/*Enable*/
        /* BIT  Output Parameter */
        /* BIT  static local */
} pid_para_Typ;
/* prototype for function block call out of Automation Studio C */
void PID_PARA (pid_para_Typ* param);




/********************************************************************************/
/* Declarations for hybridlibrary creation                                      */
/********************************************************************************/

/* 1.) pid */
/* Definition der Byte-Struktur */
typedef struct {
	/* BYTE Input  Parameter */
	short int   W;
	short int   X;
    short int   A;
    short int   Y_man;
    short int   Y_fbk;
    unsigned char    out_mode;
    struct pid_para_typ    *para_ptr;
	/* BYTE Output Parameter */
    unsigned short    status;
	short int   Y;
    short int   I_;
    short int   D_;
    /* BYTE Static Local     */
} pid_NonBool_Typ;
/* Definition der Bit-Struktur */
typedef struct {
	/* BIT  Input  Parameter */
	unsigned char enable;
    unsigned char reset;
    /* BIT  Output Parameter */
    unsigned char E_pos;
    unsigned char E_neg;
} pid_Bool_Typ;
/* prototyping for library creation*/
/* The function is called with two seperate parameters because PG2000 calles */
/* its functions with 2 seperate Pointers, one to the non boolean an one to  */
/* the boolean part. */
void pid_fbkbody (pid_Bool_Typ* , pid_NonBool_Typ* );


/* 2.) pid_min */
/* Definition der Byte-Struktur */
typedef struct  {
    /* BYTE Input  Parameter */
    short int   W;
    short int   X;
    struct pid_para_typ    *para_ptr;
    /* BYTE Output Parameter */
    unsigned short    status;
    short int   Y;
    short int   I_;
    short int   D_;
    /* BYTE Static Local     */
} pid_min_NonBool_Typ;
/* Definition der Bit-Struktur */
typedef struct {
    /* BIT  Input  Parameter */
    unsigned char enable;
    unsigned char reset;
    /* BIT  Output Parameter */
} pid_min_Bool_Typ;
/* prototyping for library creation*/
/* The function is called with two seperate parameters because PG2000 calles */
/* its functions with 2 seperate Pointers, one to the non boolean an one to  */
/* the boolean part. */
void pid_min_fbkbody (pid_min_Bool_Typ* , pid_min_NonBool_Typ* );



/* 3.) pid_para */
/* Definition der Byte-Struktur */
typedef struct {
        /* BYTE Input  Parameter */
        float   Ta;
        float   Y_max;
        float   Y_min;
        float   dY_max;
        float   Kp;
        float   Tn;
        float   Tv;
        float   Tf;
        float   Kw;
        float   Kr;
        float   e_pos;
        float   e_neg;
        float   Td;
        unsigned char    fbk_mode;
        unsigned char    d_mode;
        struct  pid_para_typ    *para_ptr;
        /* BYTE Output Parameter */
        unsigned short    status;
        /* BYTE static local */
} pid_para_NonBool_Typ;
/* Definition der Bit-Struktur */
typedef struct {
        /* BIT  Input  Parameter */
        unsigned char enable;
        /* BIT  Output Parameter */
        /* BIT  static local */
} pid_para_Bool_Typ;
/* prototyping for library creation*/
/* The function is called with two seperate parameters because PG2000 calles */
/* its functions with 2 seperate Pointers, one to the non boolean an one to  */
/* the boolean part. */
void pid_para_fbkbody (pid_para_Bool_Typ* , pid_para_NonBool_Typ* );



#endif


/*Other structures used inside the fbks:*/


typedef struct pid_2i16_typ {
    short int   hi;
    unsigned short    lo;
    } pid_2i16_typ;

typedef struct pid_2i32_typ {
    unsigned long    lo;
    long int   hi;
    } pid_2i32_typ;




