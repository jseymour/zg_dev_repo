
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	IF ((pDiagSavePopUp = TRUE) AND (OIP.ReadPage = 3)) THEN
		pDiagSavePopUpVis := VIS;
	ELSE
		pDiagSavePopUpVis := INVIS;
	END_IF

	IF ui_SaveDiagActive THEN
		pDiagSavePopUpVis := INVIS;
		pDiagSaveActivePopUpVis := VIS;
	ELSE
		pDiagSaveActivePopUpVis := INVIS;
	END_IF
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

