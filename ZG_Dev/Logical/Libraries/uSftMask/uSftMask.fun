
FUNCTION Switch : UINT
	VAR_INPUT
		myinput : UINT;
	END_VAR
END_FUNCTION

FUNCTION Complement : UINT
	VAR_INPUT
		myinput : UINT;
	END_VAR
END_FUNCTION

FUNCTION MaskAndShiftI : USINT
	VAR_INPUT
		myinput : UINT;
		mysize : USINT;
		myshift : USINT;
	END_VAR
END_FUNCTION

FUNCTION InvMaskShiftB : USINT
	VAR_INPUT
		myinput : UINT;
		mysize : USINT;
		myshift : USINT;
	END_VAR
END_FUNCTION

FUNCTION MaskAndShiftB : USINT
	VAR_INPUT
		myinput : UINT;
		mysize : USINT;
		myshift : USINT;
	END_VAR
END_FUNCTION

FUNCTION MaskAndShift : UINT
	VAR_INPUT
		myinput : UINT;
		mysize : USINT;
		myshift : USINT;
	END_VAR
END_FUNCTION

FUNCTION InvSwitch : UINT
	VAR_INPUT
		myinput : UINT;
	END_VAR
END_FUNCTION
