/*
This is an of a simple mask and shift function.  
** Author:		Paul Webber
** Company:		Piedmont Automation
** Date:		October 7, 2009
*/
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <standard.h>
#include <sys_lib.h>
#include <uSftMask.h>

UINT Switch(UINT myinput){
		return (myinput<<8) + (myinput>>8);
}

UINT Complement(UINT myinput){
		return ~myinput;
}

UINT MaskAndShift(UINT myinput, USINT mysize, USINT myshift){
		return (Switch(myinput) & ((~((~0)<<mysize))<<myshift))>>myshift;	
}

USINT MaskAndShiftB(UINT myinput, USINT mysize, USINT myshift){
		return (Switch(myinput) & ((~((~0)<<mysize))<<myshift))>>myshift;	
}

USINT InvMaskShiftB(UINT myinput, USINT mysize, USINT myshift){
		return (Switch(Complement(myinput)) & ((~((~0)<<mysize))<<myshift))>>myshift;	
}

USINT MaskAndShiftI(UINT myinput, USINT mysize, USINT myshift){
		return (Switch(myinput) & ((~((~0)<<mysize))<<myshift))>>myshift;	
}

UINT InvSwitch(UINT myinput){
		return ~Switch(myinput);
}

