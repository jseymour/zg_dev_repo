/*#ifndef	_MX_H

#define	_MX_H 1
*/
/*===========================================================================
MX Valve Polling Implementation:
===========================================================================*/
#define MX_INITIALIZE 		0
#define MX_GETVALVE			1
#define MX_CHANGE_CH		2
#define MX_POLL_CH1 		10
#define MX_POLL_CH2			20
#define MX_RESULT_CH1		11
#define MX_RESULT_CH2		21
#define MX_CMD_CH1			12
#define MX_CMD_CH2			22
#define MX_CMD_RESULT_CH1	13
#define MX_CMD_RESULT_CH2	23
#define MX_POLL_MOVE_CH1 	14
#define MX_POLL_MOVE_CH2 	24
#define MX_MOVE_RESULT_CH1	15
#define MX_MOVE_RESULT_CH2	25

/* MX Valve Position States */
#define MX_OPENED			1
#define MX_CLOSED			2
#define MX_STOPPED			4
#define MX_OPENING			8		
#define MX_CLOSING			16


/*===========================================================================
IQ Valve Polling Implementation:
===========================================================================*/
#define IQ_INITIALIZE 		0
#define IQ_GETVALVE			1
#define IQ_POLL_02	 		10
#define IQ_POLL_04			12
#define IQ_RESULT_02		11
#define IQ_RESULT_04		13
#define IQ_CMD				14
#define IQ_CMD_RESULT 		15
#define IQ_POLL_MOVE_02 	16
#define IQ_POLL_MOVE_04 	18
#define IQ_MOVE_RESULT_02	17
#define IQ_MOVE_RESULT_04	19


/*===========================================================================
IQ2 Valve Polling Implementation:
===========================================================================*/
#define IQ_CHANGE_CH			2
#define IQ_POLL_CH1_02	 		10
#define IQ_POLL_CH2_02	 		20
#define IQ_POLL_CH1_04			12
#define IQ_POLL_CH2_04			22
#define IQ_RESULT_CH1_02		11
#define IQ_RESULT_CH2_02		21
#define IQ_RESULT_CH1_04		13
#define IQ_RESULT_CH2_04		23
#define IQ_CMD_CH1				14
#define IQ_CMD_CH2				24
#define IQ_CMD_RESULT_CH1		15
#define IQ_CMD_RESULT_CH2		25
#define IQ_POLL_MOVE_CH1_02		16
#define IQ_POLL_MOVE_CH2_02		26
#define IQ_POLL_MOVE_CH1_04		18
#define IQ_POLL_MOVE_CH2_04		28
#define IQ_MOVE_RESULT_CH1_02	17
#define IQ_MOVE_RESULT_CH2_02	27
#define IQ_MOVE_RESULT_CH1_04	19
#define IQ_MOVE_RESULT_CH2_04	29
