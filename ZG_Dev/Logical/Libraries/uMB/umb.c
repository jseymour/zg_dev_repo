/*****************************************************************************
**************************  User Created MODBUS library **********************
*********************************** V2.04 - 05/25/07 **************************/

#include <bur/plc.h>	 	/* B&R Automation macros */
#include <bur/plctypes.h>
#include <string.h>
#include "drv_mbus.h"     	/* Header of MODBUS library */
#include "SYS_lib.h"

/* Library Header */
#include "umb.h"

/* Modbus Master: Open */
UINT uMBM_init(uMB_obj *mb, MBMOpen_typ *pOpen, MBMaster_typ *pServ, STRING device[32], STRING mode[32], STRING config[32], plctime timeout, plctime nocomm_sp1, plctime nocomm_sp2)
{
USINT i;

	/* Store Pointers */
	mb->open_adr	= (UDINT) pOpen;
	mb->master_adr	= (UDINT) pServ;

	/* Get 10ms Setpoint from x msecs */
	mb->nocomm_sp1  = (UINT) (nocomm_sp1 / 10.0);
	mb->nocomm_sp2  = (UINT) (nocomm_sp2 / 10.0);

	/* Assign Port Parameters */
	pOpen->pDevice	= (UDINT) &device[0];
	pOpen->pMode	= (UDINT) &mode[0];
	pOpen->pConfig	= (UDINT) &config[0];

	/* Assign Open Defaults: */
	pOpen->timeout	= timeout;
	pOpen->ascii	= 0;
	pOpen->enable 	= 1;

	/* Clear Array: V1.01 */
	for (i=0;i<200;i++)
		mb->RecordStatus[i] = 0;

	/* Call Modbus Master Initialization Function */
	MBMOpen(pOpen);

	/* Global Port OK Check */
	if ((pOpen->status == 0) && (pOpen->ident > 0)) {
		mb->PortOK 			= 1;
		pServ->ident 		= pOpen->ident;
		pServ->enable		= 1;
		/* Misc: Initilize */
		mb->_error 			= 1;
		mb->GoodMsg			= 0;
		mb->BadMsg			= 0;
		mb->ErrorNumber		= 0;
	}
	/* Port Bad */
	else {
		mb->PortOK 		= 0;
		mb->ErrorNumber	= pOpen->status;
		switch (pOpen->status) {

			case 8252:
				strcpy(mb->Message, "Syntax error in device description string");
			break;

			case 8253:
				strcpy(mb->Message, "Syntax error in mode description string");
			break;

			case 20220:
				strcpy(mb->Message, "Invalid value for timeout parameter");
			break;

			case 20240:
    			strcpy(mb->Message, "Invalid entry in data object");
    		break;

			case 20241:
				strcpy(mb->Message, "Data object not present");
			break;

   			case 20242:
    			strcpy(mb->Message, "Invalid EventPV stated in the data object");
    		break;

    		case 20243:
    			strcpy(mb->Message, "Invalid ModBus function stated in the data object");
    		break;

    		case 20244:
    			strcpy(mb->Message, "Invalid NodeNumber stated in the data object");
    		break;

    		case 20245:
    			strcpy(mb->Message, "Invalid LocalPV stated in the data object");
    		break;

			case 20246:
    			strcpy(mb->Message, "Invalid length stated in the data object");
    		break;

			case 65534:
    			strcpy(mb->Message, "Slave is not Enabled");
    		break;

			default:
			break;
		}
	}
	/* Return: Value */
	return pOpen->status;
}


/* Modbus Master: Server */
UINT uMBM_serv(uMB_obj *mb, UINT NowTime)
{
MBMaster_typ *pServ = (MBMaster_typ *) mb->master_adr;

	/* Port Opened Properly: Call Cyclic Function */
	if (mb->PortOK)
		MBMaster(pServ);
	/* Port Error: Exit */
	else {
		/* V2.01 */
		mb->CommFail		= 1;
		mb->CommFail_Crash	= 1;

		return -1;
	}
	/* Channel Has Executed */
	if (pServ->execute) {

		/* Good Communication: Good Message */
   		if (pServ->status == 0) {
   			mb->LastGoodTime  	= NowTime;
   			mb->GoodMsg++;
   			mb->ErrorNumber		= 0;
   			/* V1.01 */
   			mb->CommFail		= 0;
   			mb->CommFail_Crash	= 0;
   			/* Clear Record Fault: Check Bounds */
			if ((pServ->recordnum > 0) && (pServ->recordnum < 200))
				mb->RecordStatus[pServ->recordnum - 1] = 1;					/* V1.01 */

   			/* Good Communication: Reset Error */
   			if (mb->_error) {
   				mb->_error 		= 0;
   				strcpy(mb->Message,"Communication Active");
   			}
   		}

	}
   	/* Bad Communication: Store Status */
   	else {

		if ((pServ->status > 0) && (pServ->status != 65535)) {

			mb->ErrorNumber	= pServ->status;
			/* Update Message Failure :V1.02*/
			mb->BadMsg++;

			/* Set Record Fault: Check Bounds */
			if ((pServ->recordnum > 0) && (pServ->recordnum < 200))
   				mb->RecordStatus[pServ->recordnum - 1] = -1; 				/* V1.01 */

    		if (!mb->_error || (mb->LastBadStatus != pServ->status)) {
    			mb->_error = 1;
    			switch (pServ->status) {
    				case 20220:
    					strcpy(mb->Message, "Connection Timeout Detected");
    				break;

    				case 65534:
    					strcpy(mb->Message, "Master is not Enabled");
    				break;

    				default:
    					/* V1.01 */
    					strcpy(mb->Message, "Bad Message");
					break;
    			}
    		}
    		mb->LastBadStatus 	= pServ->status;
    	}
    }

    /* Assign */
    mb->RecordNum = pServ->recordnum;

	/* Comm Fail Timer: Comm Fail Alarm */
	if (((UINT) (NowTime - mb->LastGoodTime) > mb->nocomm_sp1))
		mb->CommFail	= 1;

	/* Comm Fail Timer: Terminal Crash */
	if (((UINT) (NowTime - mb->LastGoodTime) > mb->nocomm_sp2))
		mb->CommFail_Crash	= 1;

	/* Return: Value */
	return pServ->status;
}
/* Modbus Slave: Open */
UINT uMBS_init(uMB_obj *mb, MBSOpen_typ *pOpen, MBSlave_typ *pServ, STRING device[32], STRING mode[32], USINT own_ID, plctime timeout, plctime nocomm_sp1, plctime nocomm_sp2)
{
	/* Store Pointers */
	mb->open_adr	= (UDINT) pOpen;
	mb->master_adr	= (UDINT) pServ;

	/* Get 10ms Setpoint from x msecs */
	mb->nocomm_sp1  = (UINT) (nocomm_sp1 / 10.0);
	mb->nocomm_sp2  = (UINT) (nocomm_sp2 / 10.0);

	/* Assign Port Parameters */
	pOpen->pDevice	= (UDINT) &device[0];
	pOpen->pMode	= (UDINT) &mode[0];
	pOpen->own_ID	= own_ID;

	/* Assign Open Defaults: */
	pOpen->timeout	= timeout;

	/* Enable Function */
	pOpen->enable 	= 1;

	/* Call Modbus Slave Initialization Function */
	MBSOpen(pOpen);

	/* Global Port OK Check */
	if ((pOpen->status == 0) && (pOpen->ident > 0)) {
		mb->PortOK 			= 1;
		pServ->ident 		= pOpen->ident;
		pServ->enable		= 1;
		/* Misc: Initilize */
		mb->_error 			= 1;
   		mb->GoodMsg			= 0;
   		mb->BadMsg			= 0;
   		mb->ErrorNumber		= 0;
	}
	/* Port Bad */
	else {
		mb->PortOK 			= 0;
		mb->ErrorNumber		= pOpen->status;
		switch (pOpen->status) {

			case 8252:
				strcpy(mb->Message, "Syntax error in device description string");
			break;

			case 8253:
				strcpy(mb->Message, "Syntax error in mode description string");
			break;

			case 20205:
				strcpy(mb->Message, "Invalid own_ID stated, valid IDs are between 1 and 247");
			break;

			case 20215:
				strcpy(mb->Message, "Modbus variables MB4/MB0/MB3 are missing ");
			break;

			case 20220:
				strcpy(mb->Message, "Invalid value for timeout parameter");
			break;

			case 65534:
	    		strcpy(mb->Message, "Slave is not Enabled");
	    	break;

			default:
			break;
		}
	}
	/* Return: Value */
	return pOpen->status;

}
/* Modbus Slave: Server */
UINT uMBS_serv(uMB_obj *mb, UINT NowTime)
{
MBSlave_typ *pServ 	= (MBSlave_typ *) mb->master_adr;

	/* Port Opened Properly: Call Cyclic Function */
	if (mb->PortOK)
		MBSlave(pServ);
	/* Port Error: Exit */
	else {
		/* V2.01 */
		mb->CommFail		= 1;
		mb->CommFail_Crash	= 1;

		return -1;
	}

	/* Channel Has Executed */
	if ((pServ->exec != mb->_exec) && (pServ->status == 0)) {
   		mb->LastGoodTime  	= NowTime;
   		mb->_exec 			= pServ->exec;
   		mb->ErrorNumber		= 0;
   		mb->GoodMsg++;
   		/* V1.01 */
   		mb->CommFail		= 0;
   		mb->CommFail_Crash	= 0;
   		/* Good Communication: Reset Error */
   		if (mb->_error) {
   			mb->_error = 0;
   			strcpy(mb->Message,"Communication Active");
   		}
   	}
   	/* Bad Communication: Store Status */
   	else {
		if ((pServ->status > 0) && (pServ->status != 65535)) {
			mb->_error = 1;
			/* Update Message Failure */
			mb->BadMsg++;
			mb->ErrorNumber	= pServ->status;
    		switch (pServ->status) {
    			case 20220:
    				strcpy(mb->Message, "Connection Timeout detected");
    			break;

    			case 65534:
    				strcpy(mb->Message, "Slave is not Enabled");
    			break;

				default:
    			break;
    		}
    		mb->LastBadStatus 	= pServ->status;
    	}
    }
	/* Comm Fail Timer: Comm Fail Alarm */
	if (((UINT) (NowTime - mb->LastGoodTime) > mb->nocomm_sp1))
		mb->CommFail	= 1;

	/* Comm Fail Timer: Terminal Crash */
	if (((UINT) (NowTime - mb->LastGoodTime) > mb->nocomm_sp2))
		mb->CommFail_Crash	= 1;

	/* Return: Value */
	return pServ->status;
}

/* This Function must be called at least once every 10ms */
UINT uMB_c10ms(uMBTime_obj *t)
{
UDINT ticks = TIM_ticks();

	/* New Tick: Base */
	if (ticks != t->_ticks)
	    t->Count++;

	t->_ticks = ticks;

	/* Return Value */
	return t->Count;
}

/* V2.00 */
DINT Enraf_init (ENRAFMB_obj *e, USINT NumTanks, UDINT _enraftank_adr, uMB_obj *mb)
{

	e->_umb_adr			= (UDINT) mb;
	e->NumTanks			= NumTanks;
	e->_enraftank_adr	= _enraftank_adr;
	e->_ptr				= 0;

	/* Check Pointers */
	if (!e->_enraftank_adr | !e->_umb_adr)
		e->State		= 255;
	else
		e->State		= 0;

	return 0;
}

/* V2.01 */
DINT Enraf_serv (ENRAFMB_obj *e)
{
ENRAFTANK_obj *tank;
uMB_obj *mb;
UINT Hold = 0;
UINT Mask = 1;
UINT i;

	/* Init OK */
	if (e->State < 255) {
		/* Reference: uMB */
		mb		= (uMB_obj *) e->_umb_adr;
		/* Reference: Tanks */
		tank 	= (ENRAFTANK_obj *) e->_enraftank_adr;
		tank  	+= e->_ptr;
	}
	/* Bad Init */
	else
		return -1;

	/* State Machine */
	switch (e->State) {

		/* Poll:  */
		case 0:

			/* V1.05: Added Disable Element for Future Tanks */
			if (!tank->Disable) {
				/* Enable Message: Reset Feedback */
				tank->bPoll 	= 1;
				/* Next State: Wait */
				e->State	 	= 1;
			}
			else {
				e->State	 = 2;
				tank->CommStatus	= 0;
			}

			/* Clear Record Status */
			mb->RecordStatus[e->_ptr]	= 0;

		break;

		/* Wait Response: */
		case 1:

			/* Good Response */
			if (mb->RecordStatus[e->_ptr] == 1) {
				tank->CommStatus 	= 1;
				/* Dis-Arm: Communication Failure */
				tank->CommFail 		= 0;
				/* Next State: Wait */
				e->State 			= 2;
			}
			/* Error Response */
			else if (mb->RecordStatus[e->_ptr] == -1) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				e->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				e->State 			= 2;
			}
			/* V2.01: Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				e->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				e->State 			= 2;
			}

		break;

		/* Next Tank */
		case 2:

			/* Increment */
			e->_ptr++;
			if (e->_ptr >= e->NumTanks)
				e->_ptr = 0;

			/* Next State: Poll */
			e->State 	= 0;
		break;

		default:
		break;
	}

	/* Extract Tank Alarms */
	for (i=0;i<16;i++) {
		 Hold = tank->AlarmStatus & Mask;
		 if (Hold > 0)
		 	tank->AlarmEnrf[i] = 1;
		 else
		 	tank->AlarmEnrf[i] = 0;
	     	Mask = Mask * 2;

		/* V2.00 */
		if ((tank->AlarmEnrf[i] || tank->AlarmDI[i]) &!tank->Disable)
			tank->Alarm[i] = 1;
		else
			tank->Alarm[i] = 0;

	/* Descriptions:
		Bit 15	- Lo flow alarm
		Bit 14	- Hi flow alarm
		Bit 13	- Lo Temperature alarm
		Bit 12	- Hi Temperature alarm
		Bit 11	- Programmable LoLo Level alarm
		Bit 10	- Programmable Lo Level alarm
		Bit 9		- Programmable Hi Level alarm
		Bit 8		- Programmable HiHi Level alarm
		Bit 7		- Unauthorized movement alarms, i.e., leak alarm
		Bit 6		- Difference alarm
		Bit 5		- Variable level alarm
		Bit 4		- Floating roof alarm
		Bit 3		- Gauge LoLo alarm
		Bit 2		- Gauge Lo alarm
		Bit 1		- Gauge Hi alarm
		Bit 0		- Gauge HiHi alarm 						*/

	}

	/* Level Gauge Status */
	switch (tank->LevelStatus) {
    		case 65535:
			tank->LevelGaugeAlarm = 0;
	    		strcpy(tank->LevelMessage, "LevelGauge: Healthy");
    		break;

    		case 0:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: No Reply");
    		break;

		case 2:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Over-range");
    		break;

    		case 4:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Invalid Data");
    		break;

    		case 32:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Beyond Strapping Table");
    		break;

		default:
 			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: No Reply");
	    	break;
    	}

    	/* Temperature Gauge Status */
	switch (tank->TempStatus) {
    		case 65535:
    			tank->TempGaugeAlarm = 0;
	    		strcpy(tank->TempMessage, "TempGauge: Healthy");
    		break;

    		case 0:
     			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: No Reply");
    		break;

		case 2:
	    		tank->TempGaugeAlarm = 1;
	    		strcpy(tank->TempMessage, "TempGauge: Over-range");
    		break;

    		case 4:
	    		tank->TempGaugeAlarm = 1;
	    		strcpy(tank->TempMessage, "TempGauge: Invalid Data");
    		break;

		default:
			tank->TempGaugeAlarm = 1;
	    		strcpy(tank->TempMessage, "TempGauge: No Reply");
	    	break;
    	}

	/* V2.00: Disabled Tank: */
	if (!tank->Disable){
		/* Level -> RCA */
		tank->Level_16th = (UINT)(tank->Level * 16.0 * 12.0 + .5);

		/* FlowRate -> RCA */
		if (tank->FlowRate > 0)	{
			tank->FlowRate_i = (UINT)(INT)(tank->FlowRate + .5);
			tank->Status  = 1;
		}
		else if (tank->FlowRate < 0) {
			tank->FlowRate_i = (UINT)(INT)((tank->FlowRate - .5) * - 1.0);
			tank->Status  = 2;
		}
		else {
			tank->Status  = 3;
		}

		/* V2.00: Temperature -> RCA */
		tank->Temperature_i = (UINT)(INT)(tank->Temperature + .5);

		/* V2.00: Level Alarm: Common */
		tank->LevelAlarm = tank->Alarm[8] || tank->Alarm[9] || tank->Alarm[10] || tank->Alarm[11];

		/* V1.08: Tank Room: */
		tank->Room = tank->SafeFill - tank->TotalObservedVolume;

		/* V1.09: Product Gravity API: */
		if (tank->ObservedDensity != 0)
			tank->Gravity = (141.5/tank->ObservedDensity) - 131.5;
		else
			tank->Gravity = 0;
	}
	else {
		tank->CommFail 			= 0;
		tank->Level_16th 			= 0;
		tank->FlowRate_i 			= 0;
		tank->Temperature_i 		= 0;
		tank->Temperature 		= 0;
		tank->LevelAlarm 			= 0;
		tank->Level	 			= 0;
		tank->ObservedDensity 	= 0;
		tank->Gravity 			= 0;
		tank->VCF 				= 0;
		tank->TotalObservedVolume = 0;
		tank->Room 				= 0;
		tank->SafeFill 			= 0;
		tank->LevelGaugeAlarm 	= 0;
		tank->TempGaugeAlarm 	= 0;
		tank->UsableVolume		= 0;
		tank->MaxOperatingLevel	= 0;
		tank->LevelHiHiSP			= 0;
		tank->LevelHiSP			= 0;
		tank->LevelLowSP			= 0;
		tank->AlarmStatus			= 0;
		strcpy(tank->TempMessage, "Tank Disabled");
		strcpy(tank->LevelMessage, "Tank Disabled");
	}

	/* Return Value */
	return (DINT) e->State;
}

/* V2.03 */
DINT EnrafXS_serv (ENRAFMB_obj *e)
{
ENRAFTANKXS_obj *tank;
uMB_obj *mb;
UDINT Hold = 0;
UDINT Mask = 1;
UINT i;


	/* Init OK */
	if (e->State < 255) {
		/* Reference: uMB */
		mb		= (uMB_obj *) e->_umb_adr;
		/* Reference: Tanks */
		tank 	= (ENRAFTANKXS_obj *) e->_enraftank_adr;
		tank  	+= e->_ptr;
	}
	/* Bad Init */
	else
		return -1;

	/* State Machine */
	switch (e->State) {

		/* Poll:  */
		case 0:

			/* Added Disable Element for Future Tanks */
			if (!tank->Disable) {
				/* Enable Message: Reset Feedback */
				tank->bPoll 	= 1;
				/* Next State: Wait */
				e->State	 	= 1;
			}
			else {
				e->State	 = 2;
				tank->CommStatus	= 0;
			}

			/* Clear Record Status */
			mb->RecordStatus[e->_ptr]	= 0;

		break;

		/* Wait Response: */
		case 1:

			/* Good Response */
			if (mb->RecordStatus[e->_ptr] == 1) {
				tank->CommStatus 	= 1;
				/* Dis-Arm: Communication Failure */
				tank->CommFail 		= 0;
				/* Next State: Wait */
				e->State 			= 2;
			}
			/* Error Response */
			else if (mb->RecordStatus[e->_ptr] == -1) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				e->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				e->State 			= 2;
			}
			/* Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				e->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				e->State 			= 2;
			}

		break;

		/* Next Tank */
		case 2:

			/* Increment */
			e->_ptr++;
			if (e->_ptr >= e->NumTanks)
				e->_ptr = 0;

			/* Next State: Poll */
			e->State 	= 0;
		break;

		default:
		break;
	}


	/* Extract Tank Alarms */
	switch ((UDINT)tank->GaugeHWAlarms) {

		case 45:
			tank->GaugeHardwareAlarm = 0;
	    		strcpy(tank->GaugeMessage, "Gauge: Healthy");
    		break;

    		case 66:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: Gauge is Blocked");
    		break;

		case 67:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: Gauge is at Motor Limit");
    		break;

    		case 70:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: Data is Invalid - Not Healthy");
    		break;

    		case 72:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: High Level Alarm");
    		break;

		case 75:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: Gauge is Killed, Offline, in Maintenance");
    		break;

    		case 76:
    			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: Low Level Alarm");
    		break;

		default:
 			tank->GaugeHardwareAlarm = 1;
    			strcpy(tank->GaugeMessage, "GaugeHardwareAlarm: No Reply");
	    	break;
    	}

	/* Level Gauge Status */
	switch ((UDINT)tank->LevelStatus) {

		case 45:
			tank->LevelGaugeAlarm = 0;
	    		strcpy(tank->LevelMessage, "LevelGauge: Healthy");
    		break;

    		case 38:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Data is Manually Overwritten");
    		break;

		case 35:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Weights & Measures, Not Calibrated");
    		break;

    		case 66:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Gauge is Blocked");
    		break;

    		case 67:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Gauge is at Motor Limit");
    		break;

		case 69:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Value is Estimated");
    		break;

    		case 70:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Data is Invalid - Not Healthy");
    		break;

    		case 104:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Data is Overwritten by Host");
    		break;

		case 75:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Gauge is Killed, Offline, in Maintenance");
    		break;

		case 76:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Gauge is in Locktest");
    		break;

    		case 83:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Data is Stored During Dipping");
    		break;

    		case 84:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Gauge is in Test Mode");
    		break;

		case 63:
    			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: Data Value is at Reduced Accuracy");
    		break;

		default:
 			tank->LevelGaugeAlarm = 1;
    			strcpy(tank->LevelMessage, "LevelGauge: No Reply");
	    	break;
    	}

    	/* Temperature Gauge Status */
	switch ((UDINT)tank->TempStatus) {

		case 45:
			tank->TempGaugeAlarm = 0;
	    		strcpy(tank->TempMessage, "TempGauge: Healthy");
    		break;

    		case 38:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Data is Manually Overwritten");
    		break;

		case 35:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Weights & Measures, Not Calibrated");
    		break;

    		case 66:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Gauge is Blocked");
    		break;

    		case 67:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Gauge is at Motor Limit");
    		break;

		case 69:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Value is Estimated");
    		break;

    		case 70:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Data is Invalid - Not Healthy");
    		break;

    		case 104:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Data is Overwritten by Host");
    		break;

		case 75:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Gauge is Killed, Offline, in Maintenance");
    		break;

		case 76:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Gauge is in Locktest");
    		break;

    		case 83:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Data is Stored During Dipping");
    		break;

    		case 84:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Gauge is in Test Mode");
    		break;

		case 63:
    			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: Data Value is at Reduced Accuracy");
    		break;

		default:
 			tank->TempGaugeAlarm = 1;
    			strcpy(tank->TempMessage, "TempGauge: No Reply");
	    	break;
    	}

	/* Extract Tank Alarms */
	for (i=0;i<15;i++) {
		 Hold = tank->AlarmStatus & Mask;
		 if (Hold > 0)
		 	tank->AlarmEnrf[i] = 1;
		 else
		 	tank->AlarmEnrf[i] = 0;

		Mask = Mask * 2;

	/* Descriptions:
		Bit 15	- Un-Assigned
		Bit 14	- Un-Assigned
		:
		:
		Bit 8		- Un-Assigned
		Bit 7		- Un-Assigned
		Bit 6		- Un-Assigned
		Bit 5		- Un-Assigned
		Bit 4		- Un-Assigned
		Bit 3		- Low-Low Level Alarm
		Bit 2		- Low Level Alarm
		Bit 1		- High Level Alarm
		Bit 0		- High-High Level Alarm (0=ON, 1=OFF) 						*/

	}

	/* Disabled Tank: */
	if (!tank->Disable){

		/* V2.03: Extract High High Tank Level Alarms */
		if ((tank->AlarmEnrf[0] & !tank->CommFail) || tank->LSHH)
			tank->HighHighLevelAlarm   = 1;
		else
			tank->HighHighLevelAlarm   = 0;

		/* V2.03: Extract High Tank Level Alarms */
		if (tank->AlarmEnrf[1] || tank->LSH)
			tank->HighLevelAlarm   = 1;
		else
			tank->HighLevelAlarm   = 0;

		/* V2.03: Extract Low Tank Level Alarms */
		if (tank->AlarmEnrf[2] || tank->LSL)
			tank->LowLevelAlarm   = 1;
		else
			tank->LowLevelAlarm   = 0;

		/* V2.03: Extract Low-Low Tank Level Alarms */
		if (tank->AlarmEnrf[3] || tank->LSLL)
			tank->LowLowLevelAlarm   = 1;
		else
			tank->LowLowLevelAlarm   = 0;

		/* V2.03: Level Alarm Common */
		tank->LevelAlarm = tank->HighHighLevelAlarm || tank->HighLevelAlarm || tank->LowLevelAlarm || tank->LowLowLevelAlarm;

		/* Level -> RCA */
		tank->Level_16th = (UINT)(tank->Level * 16.0 * 12.0 + .5);

		/* FlowRate -> RCA */
		if (tank->FlowRate > 0)	{
			tank->FlowRate_i = (UINT)(INT)(tank->FlowRate + .5);
			tank->FlowStatus  = FLOW_IN;
		}
		else if (tank->FlowRate < 0) {
			tank->FlowRate_i = (UINT)(INT)((tank->FlowRate - .5) * - 1.0);
			tank->FlowStatus  = FLOW_OUT;
		}
		else {
			tank->FlowStatus  = IDLE;
		}

		/* Temperature -> RCA */
		tank->Temperature_i = (UINT)(INT)(tank->Temperature + .5);

		/* Tank Room: */
		tank->Room = tank->SafeFill - tank->TotalObservedVolume;

		/* Product Gravity API: */
		if (tank->ObservedDensity != 0)
			tank->Gravity = (141.5/tank->ObservedDensity) - 131.5;
		else
			tank->Gravity = 0;
	}
	else {
		tank->AlarmStatus				= 0;
		tank->AlarmStatus2			= 0;
		tank->CommFail 				= 0;
		tank->Level_16th 				= 0;
		tank->FlowRate_i 				= 0;
		tank->Temperature_i 			= 0;
		tank->Temperature 			= 0;
		tank->LevelAlarm 				= 0;
		tank->HighHighLevelAlarm   		= 0;
		tank->HighLevelAlarm   			= 0;
		tank->LowLevelAlarm   			= 0;
		tank->LowLowLevelAlarm   		= 0;
		tank->Level	 				= 0;
		tank->ObservedDensity 		= 0;
		tank->Gravity 				= 0;
		tank->VCF 					= 0;
		tank->TotalObservedVolume 	= 0;
		tank->GrossObservedVolume 	= 0;
		tank->GrossStandardVolume 	= 0;
		tank->Room 					= 0;
		tank->SafeFill 				= 0;
		tank->LevelGaugeAlarm 		= 0;
		tank->TempGaugeAlarm 		= 0;
		tank->GaugeHardwareAlarm 		= 0;
		tank->UsableVolume			= 0;
		tank->FlowStatus  			= 0;
		strcpy(tank->GaugeMessage, "Tank Disabled");
		strcpy(tank->TempMessage, "Tank Disabled");
		strcpy(tank->LevelMessage,	"Tank Disabled");
	}

	/* Return Value */
	return (DINT) e->State;
}
/* V2.00 */
DINT LNJ_init (LNJMB_obj *l, USINT NumTanks, UDINT _tank_adr, uMB_obj *mb, USINT ReadTyp)
{

	l->_umb_adr			= (UDINT) mb;
	l->NumTanks			= NumTanks;
	l->_tank_adr			= _tank_adr;
	l->_tank_ptr			= 0;
	l->_rec_ptr			= 0;
	l->ReadType			= ReadTyp;

	/* Check Pointers */
	if (!l->_tank_adr | !l->_umb_adr)
		l->State		= 255;
	else
		l->State		= 0;

	return 0;
}

/* V2.00 */
DINT LNJ_serv (LNJMB_obj *l)
{
LNJTANK_obj *tank;
uMB_obj *mb;
UINT Hold = 0;
UINT Mask = 1;
UINT i;

	/* Init OK */
	if (l->State < 255) {
		/* Reference: uMB */
		mb		= (uMB_obj *) l->_umb_adr;
		/* Reference: Tanks */
		tank 	= (LNJTANK_obj *) l->_tank_adr;
		tank  	+= l->_tank_ptr;
	}
	/* Bad Init */
	else
		return -1;

	/* State Machine */
	switch (l->State) {

		/* Poll:  */
		case 0:

			/* Added Disable Element for Future Tanks */
			if (!tank->Disable) {
				/* Poll Record */
				tank->bPoll[0]	= 1;
				/* Next State: Wait */
				l->State			= 1;
			}
			else {
				l->State= 5;
				tank->CommStatus = 0;
			}

			/* Clear Record Status */
			mb->RecordStatus[l->_rec_ptr]	= 0;

		break;

		/* Check Feedback */
		case 1:
			if (!tank->bPoll[0]){
				l->State		= 2;
			}
			/* V2.01: Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 				= 5;
			}
		break;

		/* Wait Response: */
		case 2:

			/* Good Response */
			if (mb->RecordStatus[l->_rec_ptr] == 1) {
				tank->CommStatus 	= 1;
				/* Dis-Arm: Communication Failure */
				tank->CommFail 		= 0;

				if (l->ReadType == 1) {
					/* Clear Record Status */
					mb->RecordStatus[l->_rec_ptr + 1] = 0;
					/* Poll Record */
					tank->bPoll[1]	= 1;
					/* Next State: Wait */
					l->State 			= 3;
				}
				else {
					/* Poll Record */
					tank->bPoll[1]	= 0;
					/* Next State: Wait */
					l->State 			= 5;
				}
			}
			/* Error Response */
			else if (mb->RecordStatus[l->_rec_ptr] == -1) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 				= 5;
			}
			/* V2.01: Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 				= 5;
			}
		break;

		/* Check Feedback */
		case 3:
			if (!tank->bPoll[1]){
				l->State		= 4;
			}
			/* V2.01: Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 				= 5;
			}
		break;

		/* Wait Response2: */
		case 4:

			/* Good Response2 */
			if (mb->RecordStatus[l->_rec_ptr + 1] == 1) {
				tank->CommStatus 	= 1;
				/* Dis-Arm: Communication Failure */
				tank->CommFail 		= 0;
				/* Next State: Wait */
				l->State 			= 5;
			}

			/* Error Response2 */
			else if (mb->RecordStatus[l->_rec_ptr + 1] == -1) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 			= 5;
			}
			/* V2.01: Comm. Fail */
			else if (mb->CommFail) {
				tank->CommStatus	= 0;
				/* Arm: Communication Failure */
				tank->CommFail 		= 1;
				/* Mark Bad Status */
				l->Status			= mb->LastBadStatus;
				/* Next State: Wait */
				l->State 				= 5;
			}
		break;

		/* Next Tank */
		case 5:

			/* Increment */
			l->_tank_ptr++;
			if (l->ReadType == 1)
				l->_rec_ptr = l->_rec_ptr + 2;
			else
				l->_rec_ptr++;

			if (l->_tank_ptr >= l->NumTanks){
				l->_tank_ptr = 0;
				l->_rec_ptr = 0;
			}

			/* Next State: Poll */
			l->State 	= 0;
		break;


		default:
		break;
	}

	/* Extract Tank Alarms */
	for (i=0;i<16;i++) {
		Hold = tank->AlarmStatus & Mask;
		if (Hold > 0)
			tank->AlarmLNJ[i] = 1;
		else
			tank->AlarmLNJ[i] = 0;
		 Mask = Mask * 2;

		if ((tank->AlarmLNJ[i] || tank->AlarmDI[i]) &!tank->Disable)
			tank->Alarm[i] = 1;
		else
			tank->Alarm[i] = 0;

		/* Descriptions:
		Bit 0		- * Low-Low Product Level Alarm not Acknowledged
		Bit 1		- * Low Product Level Alarm not Acknowledged
		Bit 2		- * High Product Level Alarm not Acknowledged
		Bit 3		- * High-High Product Level Alarm not Acknowledged
		Bit 4		- Low-Low Product Level Alarm Active
		Bit 5		- Low Product Level Alarm Active
		Bit 6		- High Product Level Alarm Active
		Bit 7		- High-High Product Level Alarm Active
		Bit 8		- Low Temperature Alarm Active
		Bit 9		- High Temperature Alarm Active
		Bit 10	- Low Rate Flow Alarm Active
		Bit 11	- High Rate Flow Alarm Active
		Bit 12	- Low Pressure Alarm Active
		Bit 13	- High Pressure Alarm Active
		Bit 14	- Spare Always 0
		Bit 15	- Spare Always 0 			*/

	}

	/* Level Gauge Communication Status */
	switch (tank->Level_32nd) {
    		case 65535:
			tank->LevelGaugeCommFail = 1;
    		break;

		default:
			tank->LevelGaugeCommFail = 0;
	    	break;
    	}

    /* Temperature Gauge Communication Status */
	switch (tank->Temperature) {
    		case 32767:
    			tank->TempGaugeCommFail = 1;
    		break;

  		default:
			tank->TempGaugeCommFail = 0;
	    	break;
    	}

	/* Level  16th of Inch*/
	if (!tank->Disable){
		tank->Level_16th = (UINT)(tank->Level_32nd/2);
	}
	else {
		tank->Level_32nd 			= 0;
		tank->Level_16th 			= 0;
		tank->APIGravity 			= 0;
		tank->GaugeInfo 			= 0;
		tank->GrossVolume 		= 0;
		tank->NetVolume 			= 0;
		tank->Temperature		= 0;
		tank->LevelGaugeCommFail 	= 0;
		tank->TempGaugeCommFail 	= 0;
		tank->CommFail 			= 0;
		tank->AlarmStatus			= 0;
	}

	/* Return Value */
	return (DINT) l->State;
}


/* EOF */



