FUNCTION LNJ_serv : DINT 
VAR_INPUT
		l	:LNJMB_obj;	
	END_VAR
END_FUNCTION
FUNCTION Enraf_init : DINT 
VAR_INPUT
		e	:ENRAFMB_obj;	
		NumTanks	:USINT;	
		_enraftank_adr	:UDINT;	
		mb	:uMB_obj;	
	END_VAR
END_FUNCTION
FUNCTION uMBS_init : UINT 
VAR_INPUT
		uMB	:uMB_obj;	
		pOpen	:MBSOpen;	
		pServ	:MBSlave;	
		device	:STRING[32];	
		mode	:STRING[32];	
		own_ID	:USINT;	
		timeout	:TIME;	
		nocomm_sp1	:TIME;	
		nocomm_sp2	:TIME;	
	END_VAR
END_FUNCTION
FUNCTION uMB_c10ms : UINT 
VAR_INPUT
		t	:uMBTime_obj;	
	END_VAR
END_FUNCTION
FUNCTION Enraf_serv : DINT 
VAR_INPUT
		e	:ENRAFMB_obj;	
	END_VAR
END_FUNCTION
FUNCTION uMBM_serv : UINT 
VAR_INPUT
		uMB	:uMB_obj;	
		c10ms	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION uMBS_serv : UINT 
VAR_INPUT
		uMB	:uMB_obj;	
		c10ms	:UINT;	
	END_VAR
END_FUNCTION
FUNCTION uMBM_init : UINT 
VAR_INPUT
		uMB	:uMB_obj;	
		pOpen	:MBMOpen;	
		pServ	:MBMaster;	
		device	:STRING[32];	
		mode	:STRING[32];	
		config	:STRING[32];	
		timeout	:TIME;	
		nocomm_sp1	:TIME;	
		nocomm_sp2	:TIME;	
	END_VAR
END_FUNCTION
FUNCTION LNJ_init : DINT 
VAR_INPUT
		l	:LNJMB_obj;	
		NumTanks	:USINT;	
		_tank_adr	:UDINT;	
		mb	:uMB_obj;	
		ReadTyp	:USINT;	
	END_VAR
END_FUNCTION
FUNCTION EnrafXS_serv : DINT 
VAR_INPUT
		e	:ENRAFMB_obj;	
	END_VAR
END_FUNCTION
