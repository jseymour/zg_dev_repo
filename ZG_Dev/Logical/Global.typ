
TYPE
	Tank_typ : 	STRUCT 
		Booster : PUMP_obj;
		Valve : ARRAY[0..1]OF VALVE_obj;
		HIHI : BOOL;
		XS : BOOL;
		AmpObject : ANALOG_obj;
		AMPS : INT;
		Low_Flow_alm : BOOL;
		ON : BOOL;
		LOW_FLOW : BOOL;
		CV_OPEN : BOOL;
		VIB : BOOL;
		START_CMD : BOOL;
		STOP_CMD : BOOL;
		Start : BOOL;
		Start_mbs : BOOL;
		Stop : BOOL;
		Stop_mbs : BOOL;
		CV_status : BOOL;
		LocalRemote : BOOL;
		OFF : BOOL;
		CVA_CLOSED : BOOL;
		CVB_CLOSED : BOOL;
		COMM_OK : BOOL;
		Bypass : BOOL;
		PRESS_SW : BOOL;
		PAL : BOOL;
	END_STRUCT;
	NodeStatus_typ : 	STRUCT 
		BC8083 : BOOL;
		PS9400 : BOOL;
		DI9371 : ARRAY[0..9]OF BOOL;
		DO9322 : ARRAY[0..2]OF BOOL;
		AI4622 : BOOL;
	END_STRUCT;
	AGWAY_typ : 	STRUCT 
		Booster : ARRAY[0..6]OF PUMP_obj;
		Valve : ARRAY[0..1]OF VALVE_obj;
		XS : BOOL;
		HIHI : BOOL;
	END_STRUCT;
	PLC_Log_type : 	STRUCT 
		cmdLogRead : USINT;
		cmdDetailInfo : USINT;
		cmdNextEntry : USINT;
		cmdPrevEntry : USINT;
		NumberDetailInfo : USINT;
		InfoHide : USINT;
		LogInfoString : ARRAY[0..19]OF STRING[45];
		DetailInfo : ERR_xtyp;
	END_STRUCT;
	OIP_obj : 	STRUCT 
		LifeSign : USINT;
		_LastLifeSign : USINT;
		ReadPage : USINT;
		SetPage : USINT;
		CommFail : BOOL;
		PT : TIME;
		ET : TIME;
		Disconnected : BOOL;
	END_STRUCT;
	Gravitometer_typ : 	STRUCT 
		PumpOn : BOOL;
		LowFlow : BOOL;
		CommFail : BOOL;
		_Watchdog : BOOL;
		Watchdog : BOOL;
	END_STRUCT;
	RemoteGrav_typ : 	STRUCT 
		ModuleOK : BOOL;
		PumpOn : BOOL;
		FlowSts : BOOL;
		PanHiLvl : BOOL;
		PmpStartPB : BOOL;
		PmpStartRmt : BOOL;
		PmpStartCmd : BOOL;
		PmpStopPB : BOOL;
		PmpStopRmt : BOOL;
		PmpStopCmd : BOOL;
		SolVlvOpCmd : BOOL;
		FAL : BOOL;
		ao_Gravity : INT;
		ai_Gravity : INT;
		Gravity : ANALOG_obj;
		do_Shutdown : BOOL;
		Pressure : ANALOG_obj;
		ai_Pressure : INT;
	END_STRUCT;
END_TYPE
