######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@'$(AS_BIN_PATH)/br.vc.pc.exe'
LINK:=@'$(AS_BIN_PATH)/BR.VC.Link.exe'
MODGEN:=@'$(AS_BIN_PATH)/BR.VC.ModGen.exe'
VCPL:=@'$(AS_BIN_PATH)/BR.VC.PL.exe'
VCHWPP:=@'$(AS_BIN_PATH)/BR.VC.HWPP.exe'
VCDEP:=@'$(AS_BIN_PATH)/BR.VC.Depend.exe'
VCFLGEN:=@'$(AS_BIN_PATH)/BR.VC.lfgen.exe'
VCREFHANDLER:=@'$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe'
VCXREFEXTENDER:=@'$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe'
RM=CMD /C DEL
PALFILE_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Palette.vcr
VCCFLAGS_Visu=-server -proj Visu -vc '$(AS_PROJECT_PATH)/Logical/HMI/Visu/VCObject.vc' -prj_path '$(AS_PROJECT_PATH)' -temp_path '$(AS_TEMP_PATH)' -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path '$(AS_CPU_PATH)'
VCFIRMWARE=4.33.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.33.0/SG4
VCOBJECT_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/VCObject.vc
VCSTARTUP='vcstart.br'
VCLOD='vclod.br'
VCSTPOST='vcstpost.br'
TARGET_FILE_Visu=$(AS_CPU_PATH)/Visu.br
OBJ_SCOPE_Visu=HMI
PRJ_PATH_Visu=$(AS_PROJECT_PATH)
SRC_PATH_Visu=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_Visu)/Visu
TEMP_PATH_Visu=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/Visu
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_Visu=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_Visu=$(TEMP_PATH_Visu)/libraries.vci
VC_XREF_BUILDFILE_Visu=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_Visu=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC='$(TEMP_PATH_Visu)/cpuhwc.vci'
VC_STATIC_OPTIONS_Visu='$(TEMP_PATH_Visu)/vcStaticOptions.xml'
VC_STATIC_OPTIONS_Shared='$(TEMP_PATH_Shared)/vcStaticOptions.xml'
# include Shared and Font Makefile (only once)
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCFntDat/Font_Visu.mak
ifneq ($(VCINC),1)
	VCINC=1
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VCShared.mak
endif

DEPENDENCIES_Visu=-d vcgclass -profile 'False'
DEFAULT_STYLE_SHEET_Visu='Source[local].StyleSheet[Default]'
SHARED_MODULE=$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcshared.br
LFNTFLAGS_Visu=-P '$(AS_PROJECT_PATH)' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)'
BDRFLAGS_Visu=-P '$(AS_PROJECT_PATH)' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)'

# Local Libs
LIB_LOCAL_OBJ_Visu=$(TEMP_PATH_Visu)/localobj.vca

# Hardware sources
PANEL_HW_OBJECT_Visu=$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/Visu/dis.Hardware.vco
PANEL_HW_VCI_Visu=$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/Visu/dis.Hardware.vci
PANEL_HW_SOURCE_Visu=C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/zg_dev_repo/ZG_Dev/Physical/Site/Hardware.hw 
DIS_OBJECTS_Visu=$(PANEL_HW_OBJECT_Visu) $(KEYMAP_OBJECTS_Visu)

# KeyMapping flags
$(TEMP_PATH_Visu)/dis.PS2-Keyboard.vco: $(AS_PROJECT_PATH)/Physical/Site/5APC2100_BY01_000/VC/PS2-Keyboard.dis $(PANEL_HW_SOURCE_Visu)
	$(VCHWPP) -f '$(PANEL_HW_SOURCE_Visu)' -o '$(subst .vco,.vci,$(TEMP_PATH_Visu)/dis.PS2-Keyboard.vco)' -n Visu -d Visu -pal '$(PALFILE_Visu)' -c '$(AS_CONFIGURATION)' -p '$(AS_PLC)' -ptemp '$(AS_TEMP_PLC)' -B 'I4.33' -L 'visapi: V*' -hw '$(CPUHWC)' -warninglevel 2 -so $(VC_STATIC_OPTIONS_Visu) -sos $(VC_STATIC_OPTIONS_Shared) -keyboard '$(AS_PROJECT_PATH)/Physical/Site/5APC2100_BY01_000/VC/PS2-Keyboard.dis' -fp '$(AS_VC_PATH)/Firmware/V4.33.0/SG4' -prj '$(AS_PROJECT_PATH)' -apj 'ZG_Dev' -sfas -vcob '$(VCOBJECT_Visu)'
	$(VCC) -f '$(subst .vco,.vci,$@)' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -k '$(VCVK_SOURCES_Visu)' $(VCCFLAGS_Visu) -p Visu -sfas

KEYMAP_SOURCES_Visu=$(AS_PROJECT_PATH)/Physical/Site/5APC2100_BY01_000/VC/PS2-Keyboard.dis 
KEYMAP_OBJECTS_Visu=$(TEMP_PATH_Visu)/dis.PS2-Keyboard.vco 

# All Source Objects
TXTGRP_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/InstantMessages.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HeaderBar.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/PageNames.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Buttons_PageTexts.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Languages.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/NumPad_Limits.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/httpURL_SDM.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_LocalRemote.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TextGroup_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveIName.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AcknowledgeState_short.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AlarmEvent_short.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AcknowledgeState_long.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_InstalledNotInstalled.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfoShort.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HelpURLs.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormLockout.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_TrendPV.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_URL.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitSts.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DeviceNames.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Details.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitxDetails.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HelpURLs_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitSts_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Details_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitxDetails_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfoShort_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Access.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_BoosterName.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_3.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_3.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_3.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_LogBook.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Misc2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Misc_4.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_URL_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_0_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units_1.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_3.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_2.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2_0.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2_3.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TankNames.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Tank_Auto_Man.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/USBText.txtgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt_1.txtgrp 

FNINFO_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Info.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Html_SDM.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Default.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Header.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Button.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Input.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Status.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_11.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_11_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_12.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_13.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_13_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_15.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_15_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_16.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_16_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_20_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_6_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_7_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_7_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10pxBold_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_1_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_13.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_13_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_13_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_13_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_14.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_14_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_14_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_14_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_6_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_1_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Button_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/DefaultFont.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Default_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Font_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Font_1_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Gfont1x1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Gfont2x2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Gfont4x4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Header_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Html_SDM_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Info_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Info_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Input_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Small.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Status_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10B_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10pxBold_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial12pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_1_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_6_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_1_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Info_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_0_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_3_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10pxBold_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_3_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_1_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_6_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_0_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_1_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Info_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_10_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_14_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_8_B_2_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_2_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_3_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial10pxBold_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial8pxRegular_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_0_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/Arial9pxBold_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_0_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_3_1.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_10_B_5_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_11_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_12_B_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_14_B_2.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_6_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_7_0_3.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_5.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_0_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_3_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_8_B_4.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_6.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_0_1_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_3_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_4_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/BVS_9_B_5_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/DefaultFont_0.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Fonts/A_9_B_7.fninfo 

BMINFO_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Inactive.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Latched.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_NotQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Quit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/alarm.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_checked.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/information.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/warning.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowRightGray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowUpGray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_BallGray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_200x200_round_nodiv.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_NeedleRed100x11_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackTransparent.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackgroundXGA_NoLogo.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_B.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BLANK.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Gray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_R.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFW.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_W.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_B.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Gray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_R.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFW.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_W.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PipeFlip.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmall.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmallFlip.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmall.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmallFlip.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndication.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndicationFlip.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmall.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmallFlip.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_pres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_unpres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_unpres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_unpres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_pres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_unpres_orange.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassOFF.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassON.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Pipe.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tank.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ChkVlv_Ver.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Blue.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_White.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_B.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_R.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_4.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_4.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_5.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_5.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Down.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Up.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/DownArrow.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_pres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_unpres.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_FLIP.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_FLIP.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Prover.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Refresh4.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/RightArrow.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tanks.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/UpArrow.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_4.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0_0.bminfo 

BMGRP_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmAcknowledgeState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmBypassState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/GlobalArea.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/msgBox.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_VlvVer1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PumFlip.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_VlvHor.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/GlobalArea_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Buttons_Color.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Close_Icons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Details_blue.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Frames.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_Arrows.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_arrows_buttons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Param_Details.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_scroll.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/control_header.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Menu.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access_Icons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PmpReg.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_ValveVer.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AccesButtons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access_Button_Group.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_CheckMark.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Valves_Horizontal.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Valves_Vertical.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Control_Bitmap_17.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/TrendButtons.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_2.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent_3.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent_4.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_CheckMark_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PUMP.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Access_Details.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_Arrows_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_2.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_1.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1_0.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_2.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/NumPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1_1.bmgrp 

PAGE_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page1_Control.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page0_Access.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page4_DateTime.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page2_Alarms.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page10_SDM.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page15_Comm.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page5_Diagnose.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page9_Clean.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page3_History.page 

LAYER_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/globalArea.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/msgBox.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Background.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/CommonLayer_1.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/CommonLayer_2.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/AlarmHeader.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Counters.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/UnitControl.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/UnitDetails.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/VlvControlPopUp.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Menu.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DischrgePress.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DischrgePress_1.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/LoopParams.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/PmpControlPopUp.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DiagSaveActivePopUp.layer \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DiagSavePopUp.layer 

VCS_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/StyleSheets/Default.vcs 

BDR_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Decrease.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Decrease_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Global_Area.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Global_Area_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Increase.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Increase_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Down.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Down_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Up.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Up_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Radio.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Radio_selected.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Left.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Left_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Right.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Right_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenNG.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBox_checked.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Flat_black.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Flat_grey.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameHeader.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/OverdriveBorder.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ProgressBarBorder.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/RaisedInner.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Raised.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SliderBorder09.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenOuter.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Sunken.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenNGgray.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameGlobal.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameInvisible.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_KeyRingOff.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_KeyRingOn.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Bevel.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_0_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_0_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_1_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_1_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_3_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_3_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_4_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_4_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_5_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_5_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_6_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_6_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_7_Released.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Bump.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Etched.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ForceActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ForcePressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Frame_Wht.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/InputFld.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Piping.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenR1.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/frmFrameHeader.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Close_Red.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Close_Red_Pressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_Orange.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_pres_orange.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_Orange.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_pres_orange.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Trans_press.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Trans_unpress.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_blue_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_blue_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_all_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_all_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_down_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_down_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_gray_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_gray_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_blue_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_blue_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_orange_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_orange_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_small.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_small_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_save_file_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_save_file_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_up_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_up_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameHeaderBlue.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Frame_Footer_Gray.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/InfoActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_all_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_all_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_dn_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_dn_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_page_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_page_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_page_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_page_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_up_pres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_up_unpres.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/brdControlActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/brdControlPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBoxOff.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBoxOn.bdr 

TPR_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NumPad.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/AlphaPad.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NavigationPad_ver.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NavigationPad_hor.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/EditPad.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NumPad_Limits.tpr 

TDC_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/TrendData.tdc 

TRD_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/CPUTemperature.trd \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/ROOMTemperature.trd 

TRE_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/Trend_Temperature.tre 

CLM_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/TankHigh.clm \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/Inputs.clm \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/LocalRemoteBtns.clm \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/LoopBtn.clm \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/ParamLimits.clm \
	$(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/USBTextMap.clm 

VCVK_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/VirtualKeys.vcvk 

VCR_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Palette.vcr 

# Runtime Object sources
VCR_OBJECT_Visu=$(TEMP_PATH_Visu)/vcrt.vco
VCR_SOURCE_Visu=$(SRC_PATH_Visu)/package.vcp
# All Source Objects END

#Panel Hardware
$(PANEL_HW_VCI_Visu): $(PANEL_HW_SOURCE_Visu) $(VC_LIBRARY_LIST_Visu) $(KEYMAP_SOURCES_Visu)
	$(VCHWPP) -f '$<' -o '$@' -n Visu -d Visu -pal '$(PALFILE_Visu)' -c '$(AS_CONFIGURATION)' -p '$(AS_PLC)' -ptemp '$(AS_TEMP_PLC)' -B 'I4.33' -L 'visapi: V*' -verbose 'False' -profile 'False' -hw '$(CPUHWC)' -warninglevel 2 -so $(VC_STATIC_OPTIONS_Visu) -sos $(VC_STATIC_OPTIONS_Shared) -fp '$(AS_VC_PATH)/Firmware/V4.33.0/SG4' -sfas -prj '$(AS_PROJECT_PATH)' -apj 'ZG_Dev' -vcob '$(VCOBJECT_Visu)'

$(PANEL_HW_OBJECT_Visu): $(PANEL_HW_VCI_Visu) $(PALFILE_Visu) $(VC_LIBRARY_LIST_Visu)
	$(VCC) -f '$(subst .vco,.vci,$@)' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -k '$(VCVK_SOURCES_Visu)' $(VCCFLAGS_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


# Pages
PAGE_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/page., $(notdir $(PAGE_SOURCES_Visu:.page=.vco)))

$(TEMP_PATH_Visu)/page.Page1_Control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page1_Control.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page0_Access.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page0_Access.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page4_DateTime.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page4_DateTime.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page2_Alarms.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page2_Alarms.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page10_SDM.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page10_SDM.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page15_Comm.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page15_Comm.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page5_Diagnose.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page5_Diagnose.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page9_Clean.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page9_Clean.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/page.Page3_History.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Pages/Page3_History.page $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_Visu)/StyleSheets/Default.vcs' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Pages END




# Stylesheets
VCS_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/vcs., $(notdir $(VCS_SOURCES_Visu:.vcs=.vco)))

$(TEMP_PATH_Visu)/vcs.Default.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/StyleSheets/Default.vcs
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -P '$(AS_PROJECT_PATH)' -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Stylesheets END




# Layers
LAYER_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/layer., $(notdir $(LAYER_SOURCES_Visu:.layer=.vco)))

$(TEMP_PATH_Visu)/layer.globalArea.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/globalArea.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.msgBox.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/msgBox.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.Background.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Background.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.CommonLayer_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/CommonLayer_1.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.CommonLayer_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/CommonLayer_2.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.AlarmHeader.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/AlarmHeader.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.Counters.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Counters.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.UnitControl.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/UnitControl.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.UnitDetails.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/UnitDetails.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.VlvControlPopUp.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/VlvControlPopUp.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.Menu.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/Menu.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.DischrgePress.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DischrgePress.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.DischrgePress_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DischrgePress_1.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.LoopParams.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/LoopParams.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.PmpControlPopUp.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/PmpControlPopUp.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.DiagSaveActivePopUp.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DiagSaveActivePopUp.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/layer.DiagSavePopUp.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Layers/DiagSavePopUp.layer $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -ds $(DEFAULT_STYLE_SHEET_Visu) -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Layers END




# Virtual Keys
VCVK_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/vcvk., $(notdir $(VCVK_SOURCES_Visu:.vcvk=.vco)))

$(TEMP_PATH_Visu)/vcvk.VirtualKeys.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/VirtualKeys.vcvk
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas

$(VCVK_OBJECTS_Visu): $(VC_LANGUAGES_Visu)

#Virtual Keys END




# Touch Pads
TPR_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/tpr., $(notdir $(TPR_SOURCES_Visu:.tpr=.vco)))

$(TEMP_PATH_Visu)/tpr.NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NumPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/tpr.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/AlphaPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/tpr.NavigationPad_ver.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NavigationPad_ver.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/tpr.NavigationPad_hor.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NavigationPad_hor.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/tpr.EditPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/EditPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/tpr.NumPad_Limits.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TouchPads/NumPad_Limits.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu) -prj 'C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/Visu' -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Touch Pads END




# Text Groups
TXTGRP_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/txtgrp., $(notdir $(TXTGRP_SOURCES_Visu:.txtgrp=.vco)))

$(TEMP_PATH_Visu)/txtgrp.InstantMessages.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/InstantMessages.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.HeaderBar.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HeaderBar.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.PageNames.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/PageNames.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Buttons_PageTexts.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Buttons_PageTexts.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Languages.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Languages.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.NumPad_Limits.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/NumPad_Limits.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.httpURL_SDM.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/httpURL_SDM.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_LocalRemote.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_LocalRemote.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TextGroup_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TextGroup_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveIName.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveIName.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveControl.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Password.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DateTimeFormats.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmCursor.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.AcknowledgeState_short.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AcknowledgeState_short.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmSeparator.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_DateTime.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.AlarmEvent_short.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AlarmEvent_short.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.AcknowledgeState_long.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/AcknowledgeState_long.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Alarms.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_InstalledNotInstalled.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_InstalledNotInstalled.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DateTimeFormats_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsDataTypes.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsModuleInfoShort.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfoShort.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsModuleInfo.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.HelpURLs.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HelpURLs.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Password_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.SaveStatusTxt.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Alarms_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmCursor_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmSeparator_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_DateTime_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormCrash.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormLockout.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormLockout.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormalBypass.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_OpenClose.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_PID.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Param.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_RecipeCounters.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Recipe.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Steps.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_TrendPV.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_TrendPV.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_URL.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_URL.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_UnitSts.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitSts.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveControl_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DeviceNames.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DeviceNames.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_CommStatus.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Loops.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Details.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Details.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Units.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_UnitxDetails.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitxDetails.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Valves2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DateTimeFormats_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.HelpURLs_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/HelpURLs_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Password_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Alarms_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_CommStatus_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmCursor_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmSeparator_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_DateTime_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Loops_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormCrash_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormalBypass_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_OpenClose_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_PID_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Param_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_RecipeCounters_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Recipe_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Steps_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_UnitSts_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitSts_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Details_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Details_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Units_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_UnitxDetails_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_UnitxDetails_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveControl_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DateTimeFormats_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DateTimeFormats_2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsDataTypes_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsDataTypes_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsDataTypes_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsModuleInfoShort_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfoShort_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsModuleInfo_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.DiagnosticsModuleInfo_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/DiagnosticsModuleInfo_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Password_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Password_2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.SaveStatusTxt_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Access.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Access.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Alarms_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Alarms_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Alarms_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_BoosterName.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_BoosterName.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_CommStatus_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_CommStatus_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmCursor_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_3.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmCursor_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmCursor_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmSeparator_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_3.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_AlarmSeparator_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_AlarmSeparator_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_DateTime_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_3.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Control_DateTime_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Control_DateTime_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_LogBook.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_LogBook.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Loops_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Loops_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Loops_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Misc2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Misc2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Misc_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Misc_4.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormCrash_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormCrash_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_NormalBypass_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_NormalBypass_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_OpenClose_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_OpenClose_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_OpenClose_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_PID_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_PID_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Param_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Param_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Param_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_RecipeCounters_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_RecipeCounters_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_RecipeCounters_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Recipe_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Recipe_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Steps_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Steps_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_URL_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_URL_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Control_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Unit_Control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Unit_Control_0_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Units_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Units_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveControl_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_3.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_ValveControl_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_ValveControl_2.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Valves2_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2_0.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TXTGRP_Valves2_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TXTGRP_Valves2_3.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.TankNames.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/TankNames.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.Tank_Auto_Man.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/Tank_Auto_Man.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.USBText.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/USBText.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/txtgrp.SaveStatusTxt_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/TextGroups/SaveStatusTxt_1.txtgrp $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Text Groups END




# BitmapGroups
BMGRP_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/bmgrp., $(notdir $(BMGRP_SOURCES_Visu:.bmgrp=.vco)))

$(TEMP_PATH_Visu)/bmgrp.AlarmAcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmAcknowledgeState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmBypassState.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmBypassState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmEvent.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmState.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Borders.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.GlobalArea.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/GlobalArea.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Pads.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.msgBox.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/msgBox.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_VlvVer1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_VlvVer1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PumFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PumFlip.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_VlvHor.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_VlvHor.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.GlobalArea_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/GlobalArea_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Buttons_Color.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Buttons_Color.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Close_Icons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Close_Icons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Color_Buttons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Details_blue.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Details_blue.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Frames.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Frames.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Orange_Arrows.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_Arrows.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Orange_arrows_buttons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_arrows_buttons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Param_Details.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Param_Details.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_scroll.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_scroll.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Borders_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Borders_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Button_Transparents.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.control_header.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/control_header.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Menu.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Menu.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmLED.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Access.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Access_Icons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access_Icons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PmpReg.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PmpReg.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_ValveVer.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_ValveVer.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AccesButtons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AccesButtons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Pads_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Access_Button_Group.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Access_Button_Group.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AcknowledgeState_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_CheckMark.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_CheckMark.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PCVGrp.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Counters.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Start_Stop.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Valves_Horizontal.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Valves_Horizontal.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Valves_Vertical.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Valves_Vertical.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Button_Transparents_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Control_Bitmap_17.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Control_Bitmap_17.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.TrendButtons.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/TrendButtons.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PCVGrp_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Pads_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Pads_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AcknowledgeState_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Start_Stop_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Button_Transparents_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AcknowledgeState_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AcknowledgeState_2.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmEvent_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent_3.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmEvent_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmEvent_4.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmLED_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.AlarmLED_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/AlarmLED_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_CheckMark_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_CheckMark_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PCVGrp1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PCVGrp1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPGRP_PUMP.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPGRP_PUMP.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Access_Details.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Access_Details.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Color_Buttons_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Color_Buttons_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Color_Buttons_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Counters_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Counters_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Counters_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Orange_Arrows_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Orange_Arrows_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Start_Stop_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_2.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.BMPG_Start_Stop_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/BMPG_Start_Stop_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Borders_1_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1_0.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Button_Transparents_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Button_Transparents_2.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/NumPad.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bmgrp.Borders_1_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/BitmapGroups/Borders_1_1.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#BitmapGroups END




# Bitmaps
BMINFO_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/bminfo., $(notdir $(BMINFO_SOURCES_Visu:.bminfo=.vco)))

$(TEMP_PATH_Visu)/bminfo.Key_NumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_AcknowledgeReset.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassOFF.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassON.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Inactive.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Inactive.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Inactive.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Latched.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Latched.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Latched.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_NotQuit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_NotQuit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_NotQuit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Quit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Quit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Quit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Reset.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_ResetAcknowledge.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Triggered.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.alarm.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/alarm.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/alarm.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_checked.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_checked.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_checked.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_default.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_default_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_default_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.information.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/information.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/information.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.warning.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/warning.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/warning.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.frame_header.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_checked.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_AlphaPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_AlphaPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Slider_09x09.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Slider_ArrowRightGray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowRightGray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowRightGray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Slider_ArrowUpGray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowUpGray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_ArrowUpGray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Slider_BallGray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_BallGray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_BallGray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_ListPadVer.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_ListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_ListPadHor.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadHor.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_ListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_ListPadVer_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.gauge_200x200_round_nodiv.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_200x200_round_nodiv.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_200x200_round_nodiv.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.gauge_NeedleRed100x11_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_NeedleRed100x11_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/gauge_NeedleRed100x11_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_gray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_EditPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_EditPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_EditPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameInvisible.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_off.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_on.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_ready.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_error.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.BackTransparent.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackTransparent.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackTransparent.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.BackgroundXGA_NoLogo.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackgroundXGA_NoLogo.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BackgroundXGA_NoLogo.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_NumPadLimits.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_NumPadLimits_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPadLimits_pressed.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_press.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE1_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_B.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_B.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_B.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_BLANK.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BLANK.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BLANK.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_BW.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_BW_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_BW_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_BW_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Closed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_G.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_G_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_G_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Gray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Gray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Gray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_GRAY_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_GRAY_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_GRAY_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_NI.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_NI_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_NI_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_NI_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_indication.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Opened.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_R.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_R.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_R.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Sequence.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Left.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Right.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_UFG.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_UFG_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_UFG_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_UFG_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_UFG_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_UFG_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFG_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_UFW.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFW.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_UFW.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_W.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_W.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_W.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_WB.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_WB_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_WB_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_WB_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Y.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_Y_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_HOR_Y_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_HOR_Y_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Y_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Y_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Y_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_B.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_B.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_B.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_B_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_B_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_B_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_BW.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_BW_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_BW_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_BW_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_BW_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Closed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_G_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_G_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_G_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Gray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Gray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Gray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_NI.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_NI_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_NI_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_NI_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_indication.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Opened.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_R.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_R.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_R.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Sequence.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Left.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Right.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_UFG.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_UFG_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_UFG_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_UFG_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFG_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_UFG_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_UFG_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFG_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_UFW.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFW.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_UFW.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_UFW_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_UFW_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_UFW_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_W.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_W.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_W.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_W_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_W_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_W_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_W_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_WB.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_WB_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_WB_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_WB_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_WB_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Y.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_Y_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_Y_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE_VER_Y_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE_VER_Y_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Y_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Y_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Y_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.valve1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE1_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.VALVE1_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/VALVE1_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PipeFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PipeFlip.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PipeFlip.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpBlueSmall.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmall.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmall.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpBlueSmallFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmallFlip.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpBlueSmallFlip.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpGreenSmall.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmall.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmall.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpGreenSmallFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmallFlip.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpGreenSmallFlip.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpNoIndication.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndication.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndication.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpNoIndicationFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndicationFlip.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpNoIndicationFlip.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpYellowSmall.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmall.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmall.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PumpYellowSmallFlip.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmallFlip.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PumpYellowSmallFlip.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Idle_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_unpressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_Pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Date_Time_Icon.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameFooterGray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameHeaderBlue.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameInvisible_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameInvisible_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.OverrideBorder.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Slider_09x09_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Slider_09x09_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Trigger_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Trigger_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_active_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_1.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_1.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_error_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_off_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_on_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_ready_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.check_button_all_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.check_button_all_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_all_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.check_button_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.check_button_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/check_button_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_off.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_on.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_checked_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_gray_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_maintenance.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_memory.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_white.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_small.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_down_pres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_pres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_pres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_down_unpres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_unpres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_down_unpres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_small.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_unpres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_unpres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_unpres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_large.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_medium.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_small.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.frame_header_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.help_white.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.info_button.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_button.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_button.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_pressd.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_pressd.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_pressd.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active5pGray.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_small.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_small_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_unpres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_unpres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_unpres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_up_pres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_pres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_pres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_up_unpres_orange.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_unpres_orange.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_up_unpres_orange.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_Selected.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmBypassOFF.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassOFF.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassOFF.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmBypassON.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassON.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmBypassON.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.password_icon.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Pipe.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Pipe.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Pipe.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Tank.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tank.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tank.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ChkVlv_Ver.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ChkVlv_Ver.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ChkVlv_Ver.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Date_Time_Icon_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_AcknowledgeReset_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassOFF_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassON_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_ResetAcknowledge_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Reset_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Triggered_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AcknowledgeReset.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmNotQuit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmQuit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ResetAcknowledge.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_Unpress.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_GunMetal_Unpress.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_Unpress_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_press_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_GunMetal_Unpress_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_Unpress.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_press.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AcknowledgeReset_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmNotQuit_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmQuit_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_AcknowledgeReset_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassOFF_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassON_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_ResetAcknowledge_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Reset_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Triggered_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.BusyNeedle.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Idle_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_unpressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Calibrate_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.CheckMark3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Clean_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_Pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Date_Time_Icon_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameFooterGray_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameHeaderBlue_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Howto_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR3_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Bar_Grey.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_Selected_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Meter.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_Blue.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Blue.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Blue.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_Green.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_White.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_White.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_White.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_Yellow.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_B.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_B.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_B.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_G.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_R.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_R.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_R.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_Y.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_1_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ResetAcknowledge_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Closed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Opened_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Sequence_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_indication_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Left_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Right_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Closed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Opened_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Sequence_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_indication_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Left_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Right_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_blue_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_blue_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_left_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_left_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_blue_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_blue_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_right_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_right_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_in_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_in_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_out_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_out_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_error_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_off_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_on_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_ready_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_off_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_on_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_checked_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_gray_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.configure.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_maintenance_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_memory_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.fileexport.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.frame_header_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.info_button_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_button_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_button_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.password_icon_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.refresh_light_blue_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.refresh_light_blue_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.valve1_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_Unpress_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_press_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_GunMetal_Unpress_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_4.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_4.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_Unpress_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_press_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_4.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_4.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AcknowledgeReset_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmNotQuit_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmQuit_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_AcknowledgeReset_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassOFF_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassON_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_ResetAcknowledge_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Reset_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Triggered_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.BusyNeedle_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/BusyNeedle_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Idle_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_unpressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_Pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Date_Time_Icon_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameFooterGray_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameHeaderBlue_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR3_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Bar_Grey_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_Selected_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_1_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ResetAcknowledge_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Closed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Opened_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Sequence_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_control_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_indication_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Left_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Right_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Closed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Opened_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Sequence_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_control_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_indication_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Left_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Right_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_blue_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_blue_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_blue_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_left_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_left_left_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_left_left_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_blue_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_blue_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_blue_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_right_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_right_right_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_right_right_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_in_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_in_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_in_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_out_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_zoom_out_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_zoom_out_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_error_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_off_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_on_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_ready_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_off_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_on_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_checked_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_gray_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.configure_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_maintenance_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_memory_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_active_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.fileexport_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/fileexport_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.frame_header_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.info_button_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_button_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_button_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.password_icon_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.refresh_light_blue_pres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_pres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.refresh_light_blue_unpres_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/refresh_light_blue_unpres_0.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.valve1_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/valve1_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_Unpress_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_Unpress_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Blue_press_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Blue_press_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_GunMetal_Unpress_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_GunMetal_Unpress_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Locked_5.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_5.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Locked_5.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_Unpress_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_Unpress_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_Orange_press_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_Orange_press_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Access_unlocked_5.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_5.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Access_unlocked_5.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AcknowledgeReset_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AcknowledgeReset_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmNotQuit_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmNotQuit_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.AlarmQuit_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/AlarmQuit_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_AcknowledgeReset_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_AcknowledgeReset_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassOFF_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassOFF_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_BypassON_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_BypassON_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_ResetAcknowledge_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_ResetAcknowledge_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Reset_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Reset_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Alarm_Triggered_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Alarm_Triggered_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Arrow_Down.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Down.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Down.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Arrow_Up.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Up.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Arrow_Up.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Idle_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Idle_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Button_Transparent_unpressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Button_Transparent_unpressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Calibrate.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Calibrate_0_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Calibrate_0_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.CheckMark3_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/CheckMark3_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Clean.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Clean_0_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Clean_0_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Close_Icon_Red_Pressed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Close_Icon_Red_Pressed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Date_Time_Icon_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Date_Time_Icon_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_Param_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_Param_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Detail_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Detail_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.DownArrow.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/DownArrow.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/DownArrow.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameFooterGray_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameFooterGray_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.FrameHeaderBlue_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/FrameHeaderBlue_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Howto_0_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Howto_0_3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_NumPad_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Key_NumPad_1_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Key_NumPad_1_pressed.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR3_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR3_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.LEDsmall_AMBR_1_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/LEDsmall_AMBR_1_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Lockout_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Lockout_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Access_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Access_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Alarm_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Alarm_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Bar_Grey_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Bar_Grey_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_2_pres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_pres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_pres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_2_unpres.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_unpres.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_2_unpres.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Control_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Control_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_DateTime_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_DateTime_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Diagnostics_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Diagnostics_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_History_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_History_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Recipe_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Recipe_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_Selected_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_Selected_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Menu_Trends_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Menu_Trends_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Meter_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Meter_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.OverrideBorder_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/OverrideBorder_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_Green_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Green_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PCV_Yellow_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PCV_Yellow_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_G_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_G_FLIP.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_FLIP.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_G_FLIP.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_Y_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.PUMP_Y_FLIP.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_FLIP.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/PUMP_Y_FLIP.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_1_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_1_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Prover.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Prover.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Prover.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Refresh4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Refresh4.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Refresh4.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ResetAcknowledge_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ResetAcknowledge_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.RightArrow.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/RightArrow.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/RightArrow.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Start_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Start_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Stop_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Stop_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Tanks.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tanks.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Tanks.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Trigger_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_active_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Trigger_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Trigger_pressed_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.UpArrow.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/UpArrow.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/UpArrow.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Up_orange_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Up_orange_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_pres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_pres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Update_unpres_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Update_unpres_1.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Closed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Closed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Opened_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Opened_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_Sequence_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_Sequence_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_control_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_control_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_no_indication_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_no_indication_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Left_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Left_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Hor_travel_Right_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Hor_travel_Right_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Closed_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Closed_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Opened_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Opened_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_Sequence_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_Sequence_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_control_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_control_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_no_indication_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_no_indication_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Left_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Left_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.Valve_Ver_travel_Right_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/Valve_Ver_travel_Right_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_pres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_pres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_gray_unpres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_gray_unpres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_pres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_pres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_light_blue_unpres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_light_blue_unpres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_pres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_pres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.but_orange_unpres_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/but_orange_unpres_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_decrease_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_decrease_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_error_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_error_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_global_area_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_global_area_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_increase_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_increase_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_off_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_off_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_on_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_on_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_radio_selected_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_radio_selected_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_ready_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_ready_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_multi_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_multi_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_down_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_down_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_left_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_left_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_right_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_right_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.button_scroll_up_multi_pressed_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/button_scroll_up_multi_pressed_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_off_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_on_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_checked_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_checked_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_small_gray_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_small_gray_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.configure_4.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_4.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/configure_4.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_maintenance_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_maintenance_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_bitmap_memory_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_bitmap_memory_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_white_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_small_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_orange_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_orange_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_small_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_large_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_medium_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_small_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_active_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_1.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_active_6.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_pressed_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_1.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_pressed_6.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.frame_header_3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/frame_header_3.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active_orange_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.help_white_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.info_button_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/info_button_2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_blue_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_blue_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.large_orange_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/large_orange_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_pres_button_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_pres_button_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.orange_unpres_button_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/orange_unpres_button_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_pressd_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_pressd_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_pressd_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.password_icon_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/password_icon_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_all_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_all_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_check_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_check_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_dn_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_dn_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_dn_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_dn_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_dn_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_dn_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_page_up_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_page_up_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_pres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_pres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.scroll_up_up_unpres_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/scroll_up_up_unpres_2.png
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active5pGray_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_small_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed_control_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_small_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_active_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_pressed_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.ProgressBorder_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/ProgressBorder_0_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_active_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_active_0_1.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.add_pressed_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/add_pressed_0_1.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.backward_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/backward_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_off_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_off_0_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.checkbox_on_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/checkbox_on_0_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_active_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_active_0_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_pressed_0_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_pressed_0_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.control_button_white_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/control_button_white_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_active_0_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.delete_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/delete_pressed_0_0.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_control_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_active_small_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_active_small_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_control_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.down_pressed_small_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/down_pressed_small_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_down_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_down_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.end_up_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/end_up_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_large_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_large_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_medium_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_medium_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.entryfield_small_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/entryfield_small_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.error_ack_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/error_ack_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_active_6_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_active_6_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.force_pressed_6_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/force_pressed_6_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.forward_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/forward_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_active_orange_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_active_orange_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.global_area_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/global_area_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.help_white_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/help_white_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.left_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/left_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_down_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_down_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.multi_up_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/multi_up_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_down_pressd_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_down_pressd_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_left_pressd_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_left_pressd_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_right_pressd_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_right_pressd_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.overdrive_up_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/overdrive_up_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.radio_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/radio_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.reset_view_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/reset_view_pressed_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.right_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/right_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active5pGray_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active5pGray_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.tab_button_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/tab_button_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_control_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_active_small_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_active_small_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_pressed_control_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_pressed_control_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.up_small_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/up_small_pressed_0_0.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_active_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_minus_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_minus_pressed_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_active_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_active_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/bminfo.zoom_plus_pressed_0_0.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0_0.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Visu/Bitmaps/zoom_plus_pressed_0_0.jpg
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Bitmaps END




# Trend Configuration
TRE_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/tre., $(notdir $(TRE_SOURCES_Visu:.tre=.vco)))

$(TEMP_PATH_Visu)/tre.Trend_Temperature.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/Trend_Temperature.tre
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Trend Configuration END




# Trend Data
TRD_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/trd., $(notdir $(TRD_SOURCES_Visu:.trd=.vco)))

$(TEMP_PATH_Visu)/trd.CPUTemperature.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/CPUTemperature.trd
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/trd.ROOMTemperature.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/ROOMTemperature.trd
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Trend Data END




# Trend Data Configuration
TDC_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/tdc., $(notdir $(TDC_SOURCES_Visu:.tdc=.vco)))

$(TEMP_PATH_Visu)/tdc.TrendData.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/Trends/TrendData.tdc
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#Trend Data Configuration END




# ColorMap Table
CLM_OBJECTS_Visu = $(addprefix $(TEMP_PATH_Visu)/clm., $(notdir $(CLM_SOURCES_Visu:.clm=.vco)))

$(TEMP_PATH_Visu)/clm.TankHigh.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/TankHigh.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/clm.Inputs.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/Inputs.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/clm.LocalRemoteBtns.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/LocalRemoteBtns.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/clm.LoopBtn.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/LoopBtn.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/clm.ParamLimits.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/ParamLimits.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


$(TEMP_PATH_Visu)/clm.USBTextMap.vco: $(AS_PROJECT_PATH)/Logical/HMI/Visu/ColorMaps/USBTextMap.clm
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_Visu)' $(VCCFLAGS_Visu)  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas


#ColorMap Table END


#
# Borders
#
BDR_SOURCES_Visu=$(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Decrease.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Decrease_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Global_Area.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Global_Area_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Increase.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Increase_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Down.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Down_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Up.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Multi_Scroll_Up_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Radio.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Radio_selected.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Left.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Left_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Right.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Right_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenNG.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBox_checked.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Flat_black.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Flat_grey.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameHeader.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/OverdriveBorder.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ProgressBarBorder.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/RaisedInner.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Raised.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SliderBorder09.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenOuter.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Sunken.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenNGgray.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameGlobal.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameInvisible.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_KeyRingOff.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_KeyRingOn.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Bevel.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_0_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_0_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_1_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_1_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_3_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_3_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_4_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_4_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_5_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_5_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_6_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_6_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Border_7_Released.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Bump.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Etched.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ForceActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ForcePressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Frame_Wht.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/InputFld.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Piping.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/SunkenR1.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/frmFrameHeader.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Close_Red.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Close_Red_Pressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_Orange.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scoll_Up_pres_orange.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_Orange.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Scroll_Down_pres_orange.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Trans_press.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_Trans_unpress.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_blue_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_blue_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_all_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_all_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_check_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_down_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_down_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_down_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_gray_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_gray_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_blue_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_blue_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_orange_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_large_orange_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_small.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_small_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_orange_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_save_file_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_save_file_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_up_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Button_up_up_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndDownActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndDownPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndUpActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/EndUpPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/FrameHeaderBlue.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Frame_Footer_Gray.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/InfoActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/MultiScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/ScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_all_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_all_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_check_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_dn_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_dn_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_page_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_page_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_dn_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_page_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_page_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_up_pres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/Scroll_up_up_unpres.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/brdControlActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/brdControlPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBoxOff.bdr $(AS_PROJECT_PATH)/Logical/HMI/Visu/Borders/CheckBoxOn.bdr 
BDR_OBJECTS_Visu=$(TEMP_PATH_Visu)/bdr.Bordermanager.vco
$(TEMP_PATH_Visu)/bdr.Bordermanager.vco: $(BDR_SOURCES_Visu)
	$(VCC) -f '$<' -o '$@' -pkg '$(SRC_PATH_Visu)' $(BDRFLAGS_Visu) $(VCCFLAGS_Visu) -p Visu$(SRC_PATH_Visu)
#
# Logical fonts
#
$(TEMP_PATH_Visu)/lfnt.de.vco: $(TEMP_PATH_Visu)/de.lfnt $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' $(LFNTFLAGS_Visu) $(VCCFLAGS_Visu) -p Visu -sfas
$(TEMP_PATH_Visu)/lfnt.en.vco: $(TEMP_PATH_Visu)/en.lfnt $(VC_LANGUAGES_Visu)
	 $(VCC) -f '$<' -o '$@' $(LFNTFLAGS_Visu) $(VCCFLAGS_Visu) -p Visu -sfas
LFNT_OBJECTS_Visu=$(TEMP_PATH_Visu)/lfnt.de.vco $(TEMP_PATH_Visu)/lfnt.en.vco 

#Runtime Object
$(VCR_OBJECT_Visu) : $(VCR_SOURCE_Visu)
	$(VCC) -f '$<' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -sl en $(VCCFLAGS_Visu) -rt  -p Visu -so $(VC_STATIC_OPTIONS_Visu) -vcr 4330 -sfas
# Local resources Library rules
LIB_LOCAL_RES_Visu=$(TEMP_PATH_Visu)/localres.vca
$(LIB_LOCAL_RES_Visu) : $(TEMP_PATH_Visu)/Visu02.ccf

# Bitmap Library rules
LIB_BMP_RES_Visu=$(TEMP_PATH_Visu)/bmpres.vca
$(LIB_BMP_RES_Visu) : $(TEMP_PATH_Visu)/Visu03.ccf
$(BMGRP_OBJECTS_Visu) : $(PALFILE_Visu) $(VC_LANGUAGES_Visu)
$(BMINFO_OBJECTS_Visu) : $(PALFILE_Visu)

BUILD_FILE_Visu=$(TEMP_PATH_Visu)/BuildFiles.arg
$(BUILD_FILE_Visu) : BUILD_FILE_CLEAN_Visu $(BUILD_SOURCES_Visu)
BUILD_FILE_CLEAN_Visu:
	$(RM) /F /Q '$(BUILD_FILE_Visu)' 2>nul
#All Modules depending to this project
PROJECT_MODULES_Visu=$(AS_CPU_PATH)/Visu01.br $(AS_CPU_PATH)/Visu02.br $(AS_CPU_PATH)/Visu03.br $(FONT_MODULES_Visu) $(SHARED_MODULE)

# General Build rules

$(TARGET_FILE_Visu): $(PROJECT_MODULES_Visu) $(TEMP_PATH_Visu)/Visu.prj
	$(MODGEN) -so $(VC_STATIC_OPTIONS_Visu) -fw '$(VCFIRMWAREPATH)' -m $(VCSTPOST) -v V1.00.0 -f '$(TEMP_PATH_Visu)/Visu.prj' -o '$@' -vc '$(VCOBJECT_Visu)' $(DEPENDENCIES_Visu) $(addprefix -d ,$(notdir $(PROJECT_MODULES_Visu:.br=)))

$(AS_CPU_PATH)/Visu01.br: $(TEMP_PATH_Visu)/Visu01.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_Visu) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_Visu)' -f '$<' -o '$@' $(DEPENDENCIES_Visu)

$(AS_CPU_PATH)/Visu02.br: $(TEMP_PATH_Visu)/Visu02.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_Visu) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_Visu)' -f '$<' -o '$@' $(DEPENDENCIES_Visu)

$(AS_CPU_PATH)/Visu03.br: $(TEMP_PATH_Visu)/Visu03.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_Visu) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_Visu)' -f '$<' -o '$@' $(DEPENDENCIES_Visu)

# General Build rules END
$(LIB_LOCAL_OBJ_Visu) : $(TEMP_PATH_Visu)/Visu01.ccf

# Main Module
$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/Visu.vcm:
$(TEMP_PATH_Visu)/Visu.prj: $(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/Visu.vcm
	$(VCDEP) -m '$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/Visu.vcm' -s '$(AS_CPU_PATH)/VCShared/Shared.vcm' -p '$(AS_PATH)/AS/VC/Firmware' -c '$(AS_CPU_PATH)' -fw '$(VCFIRMWAREPATH)' -hw '$(CPUHWC)' -so $(VC_STATIC_OPTIONS_Visu) -o Visu -proj Visu
	$(VCPL) $(notdir $(PROJECT_MODULES_Visu:.br=,4)) Visu,2 -o '$@' -p Visu -vc 'Visu' -verbose 'False' -fl '$(TEMP_PATH_ROOT_Visu)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/Visu.vcm' -vcr '$(VCR_SOURCE_Visu)' -prj '$(AS_PROJECT_PATH)' -warningLevel2 -sfas

# 01 Module

DEL_TARGET01_LFL_Visu=$(TEMP_PATH_Visu)\Visu01.ccf.lfl
$(TEMP_PATH_Visu)/Visu01.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_Visu) $(TEMP_PATH_Visu)/Visu03.ccf $(LIB_LOCAL_RES_Visu) $(TEMP_PATH_Visu)/Visu02.ccf $(DIS_OBJECTS_Visu) $(PAGE_OBJECTS_Visu) $(VCS_OBJECTS_Visu) $(VCVK_OBJECTS_Visu) $(VCRT_OBJECTS_Visu) $(TPR_OBJECTS_Visu) $(TXTGRP_OBJECTS_Visu) $(LAYER_OBJECTS_Visu) $(VCR_OBJECT_Visu) $(TDC_OBJECTS_Visu) $(TRD_OBJECTS_Visu) $(TRE_OBJECTS_Visu) $(PRC_OBJECTS_Visu) $(SCR_OBJECTS_Visu)
	-@CMD /Q /C if exist "$(DEL_TARGET01_LFL_Visu)" DEL /F /Q "$(DEL_TARGET01_LFL_Visu)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_BMP_RES_Visu)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_LOCAL_RES_Visu)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(DIS_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .page -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCS_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .vcvk -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCRT_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(TPR_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .txtgrp -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .layer -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCR_OBJECT_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .tdc -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .trd -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(TEMP_PATH_Visu)/tre.Trend_Temperature.vco' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(SCR_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p Visu -lib '$(LIB_LOCAL_OBJ_Visu)' -P '$(AS_PROJECT_PATH)' -m 'local objects' -profile 'False' -warningLevel2 -vcr 4330 -sfas
# 01 Module END

# 02 Module

DEL_TARGET02_LFL_Visu=$(TEMP_PATH_Visu)\Visu02.ccf.lfl
$(TEMP_PATH_Visu)/Visu02.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_Visu) $(TEMP_PATH_Visu)/Visu03.ccf $(BDR_OBJECTS_Visu) $(LFNT_OBJECTS_Visu) $(CLM_OBJECTS_Visu)
	-@CMD /Q /C if exist "$(DEL_TARGET02_LFL_Visu)" DEL /F /Q "$(DEL_TARGET02_LFL_Visu)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_BMP_RES_Visu)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(BDR_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LFNT_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(CLM_OBJECTS_Visu:.vco=.vco|)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p Visu -lib '$(LIB_LOCAL_RES_Visu)' -P '$(AS_PROJECT_PATH)' -m 'local resources' -profile 'False' -warningLevel2 -vcr 4330 -sfas
# 02 Module END

# 03 Module

DEL_TARGET03_LFL_Visu=$(TEMP_PATH_Visu)\Visu03.ccf.lfl
$(TEMP_PATH_Visu)/Visu03.ccf: $(LIB_SHARED) $(SHARED_CCF) $(BMGRP_OBJECTS_Visu) $(BMINFO_OBJECTS_Visu) $(PALFILE_Visu)
	-@CMD /Q /C if exist "$(DEL_TARGET03_LFL_Visu)" DEL /F /Q "$(DEL_TARGET03_LFL_Visu)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .bmgrp -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .bminfo -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Visu/Package.vcp' -temp '$(TEMP_PATH_Visu)' -prj '$(PRJ_PATH_Visu)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p Visu -lib '$(LIB_BMP_RES_Visu)' -P '$(AS_PROJECT_PATH)' -m 'bitmap resources' -profile 'False' -warningLevel2 -vcr 4330 -sfas
# 03 Module END

# Post Build Steps

.PHONY : vcPostBuild_Visu

vcPostBuild_Visu :
	$(VCC) -pb -vcm '$(TEMP_PATH_Visu)/MODULEFILES.vcm' -fw '$(VCFIRMWAREPATH)' $(VCCFLAGS_Visu) -p Visu -vcr 4330 -sfas

# Post Build Steps END
