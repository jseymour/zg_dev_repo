UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/plclog
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/zg_dev_repo/ZG_Dev/Logical/HMI/plclog

$(AS_CPU_PATH)/plclog.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	$(AS_CPU_PATH)/plclog/plclog.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/plclog/plclog.ox' -o '$(AS_CPU_PATH)/plclog.br' -v V1.00.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -d 'runtime: V* - V*' -r Cyclic4 -p 3 -s 'HMI.plclog' -L 'AsArSdm: V*, AsBrStr: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/plclog/plclog.ox: \
	$(AS_CPU_PATH)/plclog/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/plclog/a.out' -o '$(AS_CPU_PATH)/plclog/plclog.ox' -T SG4 -r Cyclic4   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/plclog/a.out: \
	$(AS_CPU_PATH)/plclog/plclog.st.o \
	$(AS_CPU_PATH)/plclog/_bur_pvdef.st.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//plclog/plclog.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/plclog/plclog.st.o: \
	$(AS_PROJECT_PATH)/Logical/HMI/plclog/plclog.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/HMI/plclog/plclog.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.typ \
	$(AS_PROJECT_PATH)/Logical/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Global.var
	@'$(AS_BIN_PATH)/BR.AS.IecCompiler.exe' '$(AS_PROJECT_PATH)/Logical/HMI/plclog/plclog.st' -o '$(AS_CPU_PATH)/plclog/plclog.st.o'  -O '$(AS_CPU_PATH)//plclog/plclog.st.o.opt' -secret '$(AS_PROJECT_PATH)_br.as.ieccompiler.exe'

$(AS_CPU_PATH)/plclog/_bur_pvdef.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/HMI/plclog/plclog.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.typ \
	$(AS_PROJECT_PATH)/Logical/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Global.var
	@'$(AS_BIN_PATH)/BR.AS.IecCompiler.exe' '$(AS_PATH)/AS/GnuInst/V4.1.2/i386-elf/include/bur/_bur_pvdef.st' -o '$(AS_CPU_PATH)/plclog/_bur_pvdef.st.o'  -O '$(AS_CPU_PATH)//plclog/_bur_pvdef.st.opt' -secret '$(AS_PROJECT_PATH)_br.as.ieccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 

