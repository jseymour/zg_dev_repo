UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/uMB
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/uMB

$(AS_CPU_PATH)/uMB.br: \
	$(AS_PROJECT_PATH)/Logical/Libraries/uMB/ANSIC.lby \
	$(AS_CPU_PATH)/uMB/uMB.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/uMB/uMB.ox' -o '$(AS_CPU_PATH)/uMB.br' -v V2.04.1 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -d 'DRV_mbus: V* - V*,sys_lib: D2009-2-19T0:0:0 - D*' -r Library -s 'Libraries.uMB' -L 'AsArSdm: V*, AsBrStr: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/uMB/uMB.ox: \
	$(AS_CPU_PATH)/uMB/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/uMB/a.out' -o '$(AS_CPU_PATH)/uMB/uMB.ox' -T SG4 -r Library   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/uMB/a.out: \
	$(AS_CPU_PATH)/uMB/umb.c.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//uMB/uMB.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/uMB/umb.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/uMB/umb.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/uMB/uMB.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/uMB/uMB.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/DRV_mbus/DRV_mbus.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/drv_mbus.h \
	$(AS_TEMP_PATH)/Includes/DataObj.h \
	$(AS_TEMP_PATH)/Includes/runtime.h \
	$(AS_TEMP_PATH)/Includes/dvframe.h \
	$(AS_TEMP_PATH)/Includes/SYS_lib.h \
	$(AS_TEMP_PATH)/Includes/umb.h \
	$(AS_TEMP_PATH)/Includes/sys_lib.h \
	$(AS_TEMP_PATH)/Includes/DRV_mbus.h
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Libraries/uMB/umb.c' -o '$(AS_CPU_PATH)/uMB/umb.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Libraries.uMB' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Libraries/uMB' '$(AS_TEMP_PATH)/Includes/Libraries/uMB' '$(AS_TEMP_PATH)/Includes' '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _UMB_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 

