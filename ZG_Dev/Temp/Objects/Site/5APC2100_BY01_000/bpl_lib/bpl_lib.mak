UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/bpl_lib
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/bpl_lib

$(AS_CPU_PATH)/bpl_lib.br: \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/ANSIC.lby \
	$(AS_CPU_PATH)/bpl_lib/bpl_lib.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/bpl_lib/bpl_lib.ox' -o '$(AS_CPU_PATH)/bpl_lib.br' -v V5.00.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -d 'sys_lib: V0.00.0 - V*,standard: V0.01.0 - V*,AsString: V0.01.0 - V*' -r Library -s 'Libraries.bpl_lib' -L 'AsArSdm: V*, AsBrStr: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/bpl_lib/bpl_lib.ox: \
	$(AS_CPU_PATH)/bpl_lib/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/bpl_lib/a.out' -o '$(AS_CPU_PATH)/bpl_lib/bpl_lib.ox' -T SG4 -r Library   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/bpl_lib/a.out: \
	$(AS_CPU_PATH)/bpl_lib/bpl_lib.c.o \
	$(AS_CPU_PATH)/bpl_lib/sm.c.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//bpl_lib/bpl_lib.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/bpl_lib/bpl_lib.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/standard.h \
	$(AS_TEMP_PATH)/Includes/sys_lib.h \
	$(AS_TEMP_PATH)/Includes/runtime.h \
	$(AS_TEMP_PATH)/Includes/AsString.h \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/mx.h \
	$(AS_TEMP_PATH)/Includes/bpl_lib.h \
	$(AS_TEMP_PATH)/Includes/asstring.h \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/sm.h
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.c' -o '$(AS_CPU_PATH)/bpl_lib/bpl_lib.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Libraries.bpl_lib' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib' '$(AS_TEMP_PATH)/Includes/Libraries/bpl_lib' '$(AS_TEMP_PATH)/Includes' '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _BPL_LIB_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/bpl_lib/sm.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/sm.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/bpl_lib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/sys_lib/sys_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/standard.h \
	$(AS_TEMP_PATH)/Includes/bpl_lib.h \
	$(AS_TEMP_PATH)/Includes/sys_lib.h \
	$(AS_TEMP_PATH)/Includes/runtime.h \
	$(AS_TEMP_PATH)/Includes/asstring.h \
	$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/sm.h
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib/sm.c' -o '$(AS_CPU_PATH)/bpl_lib/sm.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Libraries.bpl_lib' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Libraries/bpl_lib' '$(AS_TEMP_PATH)/Includes/Libraries/bpl_lib' '$(AS_TEMP_PATH)/Includes' '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _BPL_LIB_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 

