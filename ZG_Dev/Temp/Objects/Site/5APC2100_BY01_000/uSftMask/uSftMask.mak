UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/uSftMask
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/zg_dev_repo/ZG_Dev/Logical/Libraries/uSftMask

$(AS_CPU_PATH)/uSftMask.br: \
	$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask/ANSIC.lby \
	$(AS_CPU_PATH)/uSftMask/uSftMask.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/uSftMask/uSftMask.ox' -o '$(AS_CPU_PATH)/uSftMask.br' -v V1.00.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -r Library -s 'Libraries.uSftMask' -L 'AsArSdm: V*, AsBrStr: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/uSftMask/uSftMask.ox: \
	$(AS_CPU_PATH)/uSftMask/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/uSftMask/a.out' -o '$(AS_CPU_PATH)/uSftMask/uSftMask.ox' -T SG4 -r Library   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/uSftMask/a.out: \
	$(AS_CPU_PATH)/uSftMask/maskandshift.c.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//uSftMask/uSftMask.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/uSftMask/maskandshift.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask/maskandshift.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask/uSftMask.fun \
	$(AS_TEMP_PATH)/Includes/standard.h \
	$(AS_TEMP_PATH)/Includes/sys_lib.h \
	$(AS_TEMP_PATH)/Includes/runtime.h \
	$(AS_TEMP_PATH)/Includes/uSftMask.h
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask/maskandshift.c' -o '$(AS_CPU_PATH)/uSftMask/maskandshift.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Libraries.uSftMask' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask' '$(AS_TEMP_PATH)/Includes/Libraries/uSftMask' '$(AS_TEMP_PATH)/Includes' '$(AS_PROJECT_PATH)/Logical/Libraries/uSftMask' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _USFTMASK_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 

