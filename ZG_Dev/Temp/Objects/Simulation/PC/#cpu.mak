SHELL = cmd.exe
export AS_PLC := PC
export AS_TEMP_PLC := PC
export AS_CPU_PATH := $(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_PLC)
export AS_CPU_PATH_2 := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/ZG_WithHiddenTanks/Temp//Objects/$(AS_CONFIGURATION)/$(AS_PLC)
export AS_PROJECT_CONFIG_PATH := $(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)
export AS_PROJECT_CPU_PATH := $(AS_PROJECT_CONFIG_PATH)/$(AS_PLC)
export AS_STATIC_ARCHIVES_PATH := $(AS_TEMP_PATH)/Archives/$(AS_CONFIGURATION)/$(AS_PLC)
export WIN32_AS_CPU_PATH := $(WIN32_AS_TEMP_PATH)\Objects\$(AS_CONFIGURATION)\$(AS_PLC)
export WIN32_AS_ACTIVE_CONFIG_PATH := $(WIN32_AS_PROJECT_PATH)\Physical\$(AS_CONFIGURATION)\$(AS_PLC)
export AS_FILES_TO_TRANSFER := $(AS_TEMP_PATH)/Transfer/$(AS_CONFIGURATION)/$(AS_PLC)/FilesToTransfer


CpuMakeFile: \
$(AS_CPU_PATH)/ashwd.br \
$(AS_CPU_PATH)/asfw.br \
$(AS_CPU_PATH)/sysconf.br \
$(AS_CPU_PATH)/arconfig.br \
$(AS_CPU_PATH)/ashwac.br \
$(AS_CPU_PATH)/bpl_lib.br \
$(AS_CPU_PATH)/uMB.br \
$(AS_CPU_PATH)/com_lib.br \
$(AS_CPU_PATH)/uSftMask.br \
$(AS_CPU_PATH)/ModDiag.br \
$(AS_CPU_PATH)/time.br \
$(AS_CPU_PATH)/Agway191.br \
$(AS_CPU_PATH)/Agway192.br \
$(AS_CPU_PATH)/Agway193.br \
$(AS_CPU_PATH)/FuelVlv.br \
$(AS_CPU_PATH)/Tank201.br \
$(AS_CPU_PATH)/Tank202.br \
$(AS_CPU_PATH)/Tank203.br \
$(AS_CPU_PATH)/Tank204.br \
$(AS_CPU_PATH)/Tank205.br \
$(AS_CPU_PATH)/Tank206.br \
$(AS_CPU_PATH)/Tank207.br \
$(AS_CPU_PATH)/Tank208.br \
$(AS_CPU_PATH)/Tank209.br \
$(AS_CPU_PATH)/Tank210.br \
$(AS_CPU_PATH)/Tank211.br \
$(AS_CPU_PATH)/Tank212.br \
$(AS_CPU_PATH)/Tank214.br \
$(AS_CPU_PATH)/Tank216.br \
$(AS_CPU_PATH)/Tank218.br \
$(AS_CPU_PATH)/Tank220.br \
$(AS_CPU_PATH)/Tank222.br \
$(AS_CPU_PATH)/Tank224.br \
$(AS_CPU_PATH)/Tank226.br \
$(AS_CPU_PATH)/mbm_plc.br \
$(AS_CPU_PATH)/mbs_rca.br \
$(AS_CPU_PATH)/io_monitor.br \
$(AS_CPU_PATH)/Alarm_NoOf.br \
$(AS_CPU_PATH)/Alarms.br \
$(AS_CPU_PATH)/horn.br \
$(AS_CPU_PATH)/io_diag.br \
$(AS_CPU_PATH)/visuals.br \
$(AS_CPU_PATH)/plclog.br \
$(AS_CPU_PATH)/USB_Detect.br \
$(AS_CPU_PATH)/AlarmHist.br \
$(AS_CPU_PATH)/AlarmHisVi.br \
$(AS_CPU_PATH)/version.br \
$(AS_CPU_PATH)/dm_plc.br \
$(AS_CPU_PATH)/Visu.br \
vcPostBuild_Visu \
$(AS_CPU_PATH)/iomap.br \
$(AS_CPU_PATH)/Role.br \
$(AS_CPU_PATH)/User.br \
$(AS_CPU_PATH)/TCData.br \
$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/Transfer.lst


$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/Transfer.lst: \
	FORCE
	@'$(AS_BIN_PATH)/BR.AS.FinalizeBuild.exe' '$(AS_PROJECT_PATH)/ZG_WithHiddenTanks.apj' -t '$(AS_TEMP_PATH)' -o '$(AS_BINARIES_PATH)' -c '$(AS_CONFIGURATION)' -i 'C:/BrAutomation/AS43' -S 'PC'   -A 'AR000' -pil   -swFiles '$(AS_PROJECT_PATH)/Physical/Simulation/PC/Cpu.sw' -Z 'Acp10Arnc0: 3.16.2, mapp: 1.60.0, UnitSystem: n.d, TextSystem: n.d, Connectivity: n.d, AAS: n.d' -C '/RT=1000 /AM=*' -D '/IF=COM11 /BD=57600 /PA=2 /IT=20 /RS=0' -M IA32 -T SG4

#nothing to do (just call module make files)

include $(AS_CPU_PATH)/TCData/TCData.mak
include $(AS_CPU_PATH)/User/User.mak
include $(AS_CPU_PATH)/Role/Role.mak
include $(AS_CPU_PATH)/iomap/iomap.mak
include $(AS_CPU_PATH)/Visu/Visu.mak
include $(AS_CPU_PATH)/dm_plc/dm_plc.mak
include $(AS_CPU_PATH)/version/version.mak
include $(AS_CPU_PATH)/AlarmHisVi/AlarmHisVi.mak
include $(AS_CPU_PATH)/AlarmHist/AlarmHist.mak
include $(AS_CPU_PATH)/USB_Detect/USB_Detect.mak
include $(AS_CPU_PATH)/plclog/plclog.mak
include $(AS_CPU_PATH)/visuals/visuals.mak
include $(AS_CPU_PATH)/io_diag/io_diag.mak
include $(AS_CPU_PATH)/horn/horn.mak
include $(AS_CPU_PATH)/Alarms/Alarms.mak
include $(AS_CPU_PATH)/Alarm_NoOf/Alarm_NoOf.mak
include $(AS_CPU_PATH)/io_monitor/io_monitor.mak
include $(AS_CPU_PATH)/mbs_rca/mbs_rca.mak
include $(AS_CPU_PATH)/mbm_plc/mbm_plc.mak
include $(AS_CPU_PATH)/Tank226/Tank226.mak
include $(AS_CPU_PATH)/Tank224/Tank224.mak
include $(AS_CPU_PATH)/Tank222/Tank222.mak
include $(AS_CPU_PATH)/Tank220/Tank220.mak
include $(AS_CPU_PATH)/Tank218/Tank218.mak
include $(AS_CPU_PATH)/Tank216/Tank216.mak
include $(AS_CPU_PATH)/Tank214/Tank214.mak
include $(AS_CPU_PATH)/Tank212/Tank212.mak
include $(AS_CPU_PATH)/Tank211/Tank211.mak
include $(AS_CPU_PATH)/Tank210/Tank210.mak
include $(AS_CPU_PATH)/Tank209/Tank209.mak
include $(AS_CPU_PATH)/Tank208/Tank208.mak
include $(AS_CPU_PATH)/Tank207/Tank207.mak
include $(AS_CPU_PATH)/Tank206/Tank206.mak
include $(AS_CPU_PATH)/Tank205/Tank205.mak
include $(AS_CPU_PATH)/Tank204/Tank204.mak
include $(AS_CPU_PATH)/Tank203/Tank203.mak
include $(AS_CPU_PATH)/Tank202/Tank202.mak
include $(AS_CPU_PATH)/Tank201/Tank201.mak
include $(AS_CPU_PATH)/FuelVlv/FuelVlv.mak
include $(AS_CPU_PATH)/Agway193/Agway193.mak
include $(AS_CPU_PATH)/Agway192/Agway192.mak
include $(AS_CPU_PATH)/Agway191/Agway191.mak
include $(AS_CPU_PATH)/time/time.mak
include $(AS_CPU_PATH)/ModDiag/ModDiag.mak
include $(AS_CPU_PATH)/uSftMask/uSftMask.mak
include $(AS_CPU_PATH)/com_lib/com_lib.mak
include $(AS_CPU_PATH)/uMB/uMB.mak
include $(AS_CPU_PATH)/bpl_lib/bpl_lib.mak
include $(AS_CPU_PATH)/ashwac/ashwac.mak
include $(AS_CPU_PATH)/arconfig/arconfig.mak
include $(AS_CPU_PATH)/sysconf/sysconf.mak
include $(AS_CPU_PATH)/asfw/asfw.mak
include $(AS_CPU_PATH)/ashwd/ashwd.mak

.DEFAULT:
#nothing to do (need this target for prerequisite files that don't exit)

FORCE:
