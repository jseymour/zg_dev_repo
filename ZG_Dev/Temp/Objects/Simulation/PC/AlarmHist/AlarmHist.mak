UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/ZG_WithHiddenTanks/Logical/HMI/USB_HANDLING/AlarmHist
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/ZG_WithHiddenTanks/Logical/HMI/USB_HANDLING/AlarmHist

$(AS_CPU_PATH)/AlarmHist.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	FORCE \
	$(AS_CPU_PATH)/AlarmHist/AlarmHist.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/AlarmHist/AlarmHist.ox' -o '$(AS_CPU_PATH)/AlarmHist.br' -v V1.00.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -d 'runtime: V* - V*' -r Cyclic8 -p 5 -s 'HMI.USB_HANDLING.AlarmHist' -L 'AsArSdm: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/AlarmHist/AlarmHist.ox: \
	$(AS_CPU_PATH)/AlarmHist/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/AlarmHist/a.out' -o '$(AS_CPU_PATH)/AlarmHist/AlarmHist.ox' -T SG4 -r Cyclic8   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/AlarmHist/a.out: \
	$(AS_CPU_PATH)/AlarmHist/AlarmHistCyclic.st.o \
	$(AS_CPU_PATH)/AlarmHist/_bur_pvdef.st.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//AlarmHist/AlarmHist.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/AlarmHist/AlarmHistCyclic.st.o: \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHistCyclic.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/FileIO/FileIO.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsArSdm/AsArSdm.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/visapi/Visapi.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHist.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/Variables.var \
	$(AS_PROJECT_PATH)/Logical/Global.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHist.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/runtime/runtime.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsArSdm/AsArSdm.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/Types.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/FileIO/FileIO.var
	@'$(AS_BIN_PATH)/BR.AS.IecCompiler.exe' '$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHistCyclic.st' -o '$(AS_CPU_PATH)/AlarmHist/AlarmHistCyclic.st.o'  -O '$(AS_CPU_PATH)//AlarmHist/AlarmHistCyclic.st.o.opt' -secret '$(AS_PROJECT_PATH)_br.as.ieccompiler.exe'

$(AS_CPU_PATH)/AlarmHist/_bur_pvdef.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/FileIO/FileIO.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsArSdm/AsArSdm.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/visapi/Visapi.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHist.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/Variables.var \
	$(AS_PROJECT_PATH)/Logical/Global.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/AlarmHist/AlarmHist.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/runtime/runtime.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsArSdm/AsArSdm.var \
	$(AS_PROJECT_PATH)/Logical/HMI/USB_HANDLING/Types.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/FileIO/FileIO.var
	@'$(AS_BIN_PATH)/BR.AS.IecCompiler.exe' '$(AS_PATH)/AS/GnuInst/V4.1.2/i386-elf/include/bur/_bur_pvdef.st' -o '$(AS_CPU_PATH)/AlarmHist/_bur_pvdef.st.o'  -O '$(AS_CPU_PATH)//AlarmHist/_bur_pvdef.st.opt' -secret '$(AS_PROJECT_PATH)_br.as.ieccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 



FORCE: