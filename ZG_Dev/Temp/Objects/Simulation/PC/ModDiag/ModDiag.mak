UnmarkedObjectFolder := C:/Users/jseym/Documents/Project Documents/Buckeye/17005 Macungie Tank Farm Replacement/ZG_WithHiddenTanks/Logical/Sources/Service/Diagnostics/ModDiag
MarkedObjectFolder := C:/Users/jseym/Documents/Project\ Documents/Buckeye/17005\ Macungie\ Tank\ Farm\ Replacement/ZG_WithHiddenTanks/Logical/Sources/Service/Diagnostics/ModDiag

$(AS_CPU_PATH)/ModDiag.br: \
	$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/ANSIC.lby \
	FORCE \
	$(AS_CPU_PATH)/ModDiag/ModDiag.ox
	@'$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe' '$(AS_CPU_PATH)/ModDiag/ModDiag.ox' -o '$(AS_CPU_PATH)/ModDiag.br' -v V2.05.0 -f '$(AS_CPU_PATH)/NT.ofs' -offsetLT '$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs' -T SG4  -M IA32  -B I4.33 -extConstants -d 'AsIO: V* - V*,AsIODiag: V* - V*,AsIOMMan: V* - V*,Sys_lib: V* - V*,Standard: V* - V*,DataObj: V* - V*' -r Library -s 'Sources.Service.Diagnostics.ModDiag' -L 'AsArSdm: V*, AsIO: V*, AsIODiag: V*, AsIOMMan: V*, asstring: V*, astime: V*, AsUSB: V*, bpl_lib: V5.00.0, com_lib: V3.08.0, Convert: V*, DataObj: V*, DRV_mbus: V*, DRV_mn: V*, dvframe: V*, FileIO: V*, ModDiag: V2.05.0, operator: V*, runtime: V*, standard: V*, sys_lib: V*, uMB: V2.04.1, uSftMask: V*, visapi: V*' -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.taskbuilder.exe'

$(AS_CPU_PATH)/ModDiag/ModDiag.ox: \
	$(AS_CPU_PATH)/ModDiag/a.out
	@'$(AS_BIN_PATH)/BR.AS.Backend.exe' '$(AS_CPU_PATH)/ModDiag/a.out' -o '$(AS_CPU_PATH)/ModDiag/ModDiag.ox' -T SG4 -r Library   -G V4.1.2  -B I4.33 -secret '$(AS_PROJECT_PATH)_br.as.backend.exe'

$(AS_CPU_PATH)/ModDiag/a.out: \
	$(AS_CPU_PATH)/ModDiag/iolist.c.o \
	$(AS_CPU_PATH)/ModDiag/iostatus.c.o \
	$(AS_CPU_PATH)/ModDiag/modulediagnose.c.o
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' -link  -O '$(AS_CPU_PATH)//ModDiag/ModDiag.out.opt' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/ModDiag/iolist.c.o: \
	$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/iolist.c \
	FORCE 
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/iolist.c' -o '$(AS_CPU_PATH)/ModDiag/iolist.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Sources.Service.Diagnostics.ModDiag' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _MODDIAG_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/ModDiag/iostatus.c.o: \
	$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/iostatus.c \
	FORCE 
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/iostatus.c' -o '$(AS_CPU_PATH)/ModDiag/iostatus.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Sources.Service.Diagnostics.ModDiag' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _MODDIAG_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

$(AS_CPU_PATH)/ModDiag/modulediagnose.c.o: \
	$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/modulediagnose.c \
	FORCE 
	@'$(AS_BIN_PATH)/BR.AS.CCompiler.exe' '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag/modulediagnose.c' -o '$(AS_CPU_PATH)/ModDiag/modulediagnose.c.o'  -T SG4  -M IA32  -B I4.33 -G V4.1.2  -s 'Sources.Service.Diagnostics.ModDiag' -t '$(AS_TEMP_PATH)' -specs=I386specs_brelf -r Library -I '$(AS_PROJECT_PATH)/Logical/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes/Sources/Service/Diagnostics/ModDiag' '$(AS_TEMP_PATH)/Includes' -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include '$(AS_CPU_PATH)/Libraries.h' -D _MODDIAG_EXPORT -x c -P '$(AS_PROJECT_PATH)' -secret '$(AS_PROJECT_PATH)_br.as.ccompiler.exe'

-include $(AS_CPU_PATH)/Force.mak 



FORCE: