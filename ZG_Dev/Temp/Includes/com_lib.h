/* Automation Studio generated header file */
/* Do not edit ! */
/* com_lib 3.08.0 */

#ifndef _COM_LIB_
#define _COM_LIB_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _com_lib_VERSION
#define _com_lib_VERSION 3.08.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "sys_lib.h"
		#include "asstring.h"
		#include "standard.h"
#endif
#ifdef _SG3
		#include "sys_lib.h"
		#include "asstring.h"
		#include "standard.h"
#endif
#ifdef _SGC
		#include "sys_lib.h"
		#include "asstring.h"
		#include "standard.h"
#endif

/* Constants */
#ifdef _REPLACE_CONST
 #define I386 2U
 #define M68K 1U
 #define MMDDYY 0
 #define MMDDYYYY 1
 #define NegOk 0
 #define NegVoid 1
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST unsigned char I386;
 _GLOBAL_CONST unsigned char M68K;
 _GLOBAL_CONST plcbit MMDDYY;
 _GLOBAL_CONST plcbit MMDDYYYY;
 _GLOBAL_CONST plcbit NegOk;
 _GLOBAL_CONST plcbit NegVoid;
#endif




/* Datatypes and datatypes of function blocks */
typedef struct DATESTRING_obj
{	plcstring sDate[11];
	plcstring ddString[3];
	plcstring yyString[5];
} DATESTRING_obj;

typedef struct RTCSTRING_obj
{	plcstring ddString[3];
	plcstring yyyyString[5];
	plcstring mmString[3];
	plcstring ssString[3];
	plcstring Date[11];
	plcstring Time[9];
	plcstring DateTime[15];
	plcstring _Date[9];
	plcstring _Time[7];
	plcstring _yString[5];
	plcstring _mString[3];
	plcstring _dString[3];
	plcstring _hString[3];
	plcstring _minString[3];
	plcstring _sString[3];
} RTCSTRING_obj;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit LongNoSwap(unsigned long value, unsigned long word_adr);
_BUR_PUBLIC plcbit Analog64kToUINT(unsigned long mb64k_adr, float high, float low, unsigned long out_adr);
_BUR_PUBLIC plcbit GetBitWrite(unsigned long word_adr, unsigned long bit_adr);
_BUR_PUBLIC plcbit CreateRTCString(struct RTCSTRING_obj* rtcstr, plcbit typ);
_BUR_PUBLIC plcbit CreateDateString(struct DATESTRING_obj* datestr, unsigned long in_adr, plcbit typ);
_BUR_PUBLIC plcbit GetBitLatch(unsigned long word_adr, unsigned long bit_adr);
_BUR_PUBLIC unsigned long SwapLong(unsigned short word1, unsigned short word2);
_BUR_PUBLIC plcbit LongSwap(unsigned long value, unsigned long word_adr);
_BUR_PUBLIC plcbit GetLongRead(unsigned long value_adr, unsigned long plcvalue_adr, unsigned char type);
_BUR_PUBLIC unsigned char SetBit(unsigned char x, unsigned char index, plcbit value);
_BUR_PUBLIC plcbit Trans_byte(unsigned long dest_adr, unsigned long src_adr, unsigned char length);
_BUR_PUBLIC plcbit Trans_word(unsigned long dest_adr, unsigned long src_adr);
_BUR_PUBLIC unsigned char ByteToBit(unsigned char x, unsigned long bit_adr);
_BUR_PUBLIC plcbit GetBit(unsigned long byte_adr, unsigned long bit_adr);
_BUR_PUBLIC plcbit GetWordWrite(unsigned long value_adr, unsigned long plcvalue_adr);
_BUR_PUBLIC plcbit GLW_Init(struct TON* timer, unsigned char num_long_writes, unsigned long pt);
_BUR_PUBLIC plcbit WordToByte(unsigned long byte_adr, unsigned short word);
_BUR_PUBLIC plcbit GetLongWrite(unsigned long value_adr, unsigned long plcvalue_adr, struct TON* timer, unsigned char type);


#ifdef __cplusplus
};
#endif
#endif /* _COM_LIB_ */

