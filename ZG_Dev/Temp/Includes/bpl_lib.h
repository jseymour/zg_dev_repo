/* Automation Studio generated header file */
/* Do not edit ! */
/* bpl_lib 5.00.0 */

#ifndef _BPL_LIB_
#define _BPL_LIB_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _bpl_lib_VERSION
#define _bpl_lib_VERSION 5.00.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "sys_lib.h"
		#include "asstring.h"
		#include "standard.h"
#endif



/* Constants */
#ifdef _REPLACE_CONST
 #define AI 0U
 #define AO 1U
 #define AX 2U
 #define NO 0
 #define OK 0
 #define ON 1
 #define V8 1
 #define BPH 2U
 #define CH1 0U
 #define CH2 1U
 #define GPH 1U
 #define GPM 0U
 #define OFF 0
 #define V16 0
 #define V32 2U
 #define V64 4U
 #define YES 1
 #define APID 3U
 #define FAIL 1
 #define cINT 2U
 #define mlAI 1U
 #define HW_IO 0U
 #define NOIND 0U
 #define SW_IO 1U
 #define cUINT 1U
 #define mlHSC 0U
 #define mlRCA 2U
 #define CLOSED 1U
 #define OPENED 2U
 #define TRAVEL 3U
 #define cFLOAT 3U
 #define CLOSING 4U
 #define FQ_TYPE 1U
 #define IQ_TYPE 0U
 #define OPENING 3U
 #define STOPPED 5U
 #define cmdCONT 1U
 #define IQ1_TYPE 2U
 #define cmdPULSE 0U
 #define IQ_NULL_CMD 10U
 #define IQ_OPEN_CMD 2U
 #define IQ_STOP_CMD 0U
 #define MX_OPEN_CMD 256U
 #define MX_STOP_CMD 512U
 #define IQ_CLOSE_CMD 1U
 #define MX_CLOSE_CMD 768U
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST unsigned char AI;
 _GLOBAL_CONST unsigned char AO;
 _GLOBAL_CONST unsigned char AX;
 _GLOBAL_CONST plcbit NO;
 _GLOBAL_CONST plcbit OK;
 _GLOBAL_CONST plcbit ON;
 _GLOBAL_CONST plcbit V8;
 _GLOBAL_CONST unsigned char BPH;
 _GLOBAL_CONST unsigned char CH1;
 _GLOBAL_CONST unsigned char CH2;
 _GLOBAL_CONST unsigned char GPH;
 _GLOBAL_CONST unsigned char GPM;
 _GLOBAL_CONST plcbit OFF;
 _GLOBAL_CONST plcbit V16;
 _GLOBAL_CONST unsigned char V32;
 _GLOBAL_CONST unsigned char V64;
 _GLOBAL_CONST plcbit YES;
 _GLOBAL_CONST unsigned char APID;
 _GLOBAL_CONST plcbit FAIL;
 _GLOBAL_CONST unsigned char cINT;
 _GLOBAL_CONST unsigned char mlAI;
 _GLOBAL_CONST unsigned char HW_IO;
 _GLOBAL_CONST unsigned char NOIND;
 _GLOBAL_CONST unsigned char SW_IO;
 _GLOBAL_CONST unsigned char cUINT;
 _GLOBAL_CONST unsigned char mlHSC;
 _GLOBAL_CONST unsigned char mlRCA;
 _GLOBAL_CONST unsigned char CLOSED;
 _GLOBAL_CONST unsigned char OPENED;
 _GLOBAL_CONST unsigned char TRAVEL;
 _GLOBAL_CONST unsigned char cFLOAT;
 _GLOBAL_CONST unsigned char CLOSING;
 _GLOBAL_CONST unsigned char FQ_TYPE;
 _GLOBAL_CONST unsigned char IQ_TYPE;
 _GLOBAL_CONST unsigned char OPENING;
 _GLOBAL_CONST unsigned char STOPPED;
 _GLOBAL_CONST unsigned char cmdCONT;
 _GLOBAL_CONST unsigned char IQ1_TYPE;
 _GLOBAL_CONST unsigned char cmdPULSE;
 _GLOBAL_CONST unsigned short IQ_NULL_CMD;
 _GLOBAL_CONST unsigned short IQ_OPEN_CMD;
 _GLOBAL_CONST unsigned short IQ_STOP_CMD;
 _GLOBAL_CONST unsigned short MX_OPEN_CMD;
 _GLOBAL_CONST unsigned short MX_STOP_CMD;
 _GLOBAL_CONST unsigned short IQ_CLOSE_CMD;
 _GLOBAL_CONST unsigned short MX_CLOSE_CMD;
#endif




/* Datatypes and datatypes of function blocks */
typedef struct TON_10msobj
{	unsigned long PT;
	unsigned long ET;
	unsigned long STARTTIME;
	plcbit IN;
	plcbit Q;
	plcbit M;
} TON_10msobj;

typedef struct ALARM_obj
{	plcbit IN[16];
	plcbit OUT[16];
	plcbit oldOUT[16];
	plcbit horn;
	unsigned short horn_mask;
	plcbit crash;
	unsigned short crash_mask;
	struct TON_10msobj timer;
	unsigned short group;
	unsigned short ingroup;
	unsigned short group_old;
	plcbit common;
	plcbit new;
	plcbit UnAckAlm;
	plcbit UnAckAlm_PP41;
	plcbit UnAckAlm_C100;
	plcbit AckImg_PP41[16];
	unsigned short AckImg;
	plcbit AckImg_C100[16];
} ALARM_obj;

typedef struct ANALOG_obj
{	unsigned char type;
	float high;
	float low;
	float range;
	unsigned char mult;
	plcbit fail;
	unsigned short value_64k;
	float value_f;
	signed short value_i;
	signed short value_pid;
	float Current;
	float hihi_sp;
	float high_sp;
	float low_sp;
	plcbit hihi_alarm;
	plcbit high_alarm;
	plcbit low_alarm;
	unsigned long raw_adr;
} ANALOG_obj;

typedef struct BOOSTER_obj
{	plcbit start_oip;
	plcbit start_remote;
	plcbit stop_oip;
	plcbit stop_remote;
	plcbit loreset_oip;
	plcbit StartCmd;
	plcbit StopCmd;
	plcbit MasterStart;
	plcbit Lockout;
	plcbit Shutdown;
	plcbit FAL;
	plcbit MotorOnInd;
	plcbit PortOK;
	plcbit CommFail;
	plcbit GoodMsg;
	unsigned short LastBadStatus;
	signed short MMR_read[4];
	signed short MMR_GeneralStatus;
	signed short MMR_LastTrip;
	signed short MMR_Amp;
	signed short MMR_Hours;
	plcbit MMR_Alarm;
	plcbit MotorOverload;
	unsigned short oip_status;
	plcbit Trip;
	plctime StartupOverridePT;
	plctime StartDelayPT;
	plctime LowFlowPT;
	struct TON_10msobj cmdtimer;
	struct ALARM_obj AlarmGroup;
	unsigned char State;
	unsigned char State_Error;
	unsigned long State_Timer;
	plcbit State_Timer_Done;
	unsigned long State_Timer_ET;
	unsigned long State_Timer_PT;
	plcbit State_Timer_On;
	plcbit State_Start;
	plcbit State_Stop;
	plcbit State_Complete;
	plcbit State_Alarm;
} BOOSTER_obj;

typedef struct TOF_10msobj
{	unsigned long PT;
	unsigned long ET;
	unsigned long STARTTIME;
	plcbit IN;
	plcbit Q;
	plcbit M;
} TOF_10msobj;

typedef struct CLOCK_obj
{	unsigned char String[21];
	plcbit Trigger;
	unsigned long rcatime_adr;
	unsigned char rcatime[16];
	struct TOF_10msobj timer;
} CLOCK_obj;

typedef struct DETECTOR_obj
{	unsigned short Current;
	plcbit Fault_alarm;
	plcbit Fire_alarm;
	plcstring message[19];
	struct TON_10msobj FilterTimer;
	struct TON_10msobj FaultAlarmTimer;
	struct TON_10msobj FireAlarmTimer;
	unsigned long raw_adr;
	float _raw;
	float _rawold;
} DETECTOR_obj;

typedef struct DSUMP_obj
{	plcstring Message[21];
	plcstring Name[21];
	plcbit start_oip;
	plcbit start_remote;
	plcbit stop_oip;
	plcbit stop_remote;
	plcbit toggle_oip;
	plcbit Reset_oip;
	plcbit on_led;
	plcbit off_led;
	plcbit motor_run_cmd;
	unsigned short FlowDelaySecs;
	plcbit Auto;
	plcbit Lockout;
	plcbit LL;
	plcbit HL;
	plcbit FS;
	plcbit AlarmStartFailure;
	plcbit AlarmNoFlow;
	plcbit shutof_valve_cmd;
	unsigned long security_adr;
	unsigned long terminal_adr;
	unsigned long secs_adr;
	plcbit motor_run;
	unsigned short _t0;
	unsigned short _t1;
} DSUMP_obj;

typedef struct VALVE_obj
{	plcbit enable;
	plcbit open_cmd;
	plcbit close_cmd;
	plcbit stop_cmd;
	plcbit open_oip;
	plcbit close_local;
	plcbit open_local;
	plcbit open_remote;
	plcbit close_oip;
	plcbit close_remote;
	plcbit stop_oip;
	plcbit oip_hold;
	plcbit stop_remote;
	unsigned char position;
	unsigned char percent;
	plcbit LSC;
	plcbit LSO;
	unsigned char ID;
	plcbit local;
	plcbit fault;
	plcbit Alarm[8];
	plcbit FailedToOpenAlarm;
	plcbit FailedToCloseAlarm;
	plcbit commfail;
	plcstring message[8];
	plcbit opn;
	plcbit cls;
	plcbit stp;
} VALVE_obj;

typedef struct PUMP_obj
{	plcbit Status;
	plcbit Run;
	plcbit AutoSD;
	plcbit NoFlow;
	plcbit start_oip;
	plcbit start_remote;
	plcbit start_cmd;
	plcbit stop_oip;
	plcbit stop_remote;
	plcbit stop_cmd;
	plcbit oip_hold;
	plcbit toggle_oip;
	struct TOF_10msobj starttimer;
	unsigned long status_adr;
	unsigned long run_adr;
	plcbit Auto;
	plcbit Hand;
	plcbit Overload;
	plcstring message[20];
	plcbit on_led;
	plcbit off_led;
	unsigned char StatusNum;
	plcbit Armed;
} PUMP_obj;

typedef struct FL_obj
{	plcstring message[31];
	plcbit open_remote;
	plcbit open_oip;
	plcbit close_remote;
	plcbit close_oip;
	plcbit PSL;
	plcbit LowPressure;
	plcbit ShutDown;
	plcbit open_cmd;
	plcbit close_cmd;
	plcbit oip_hold;
	struct VALVE_obj mov;
	struct PUMP_obj pump;
	struct TON_10msobj psitimer;
	struct TON_10msobj smtimer;
	unsigned char State;
	unsigned char oldState;
	unsigned long t[7];
} FL_obj;

typedef struct FSL_obj
{	plcbit MasterStart;
	plcbit _MasterStart;
	plcbit Start_Override;
	plcbit _NoFlow;
	plcbit Fsl;
	plcbit NoFlow;
	struct TOF_10msobj Timer_Override;
	struct TOF_10msobj Timer_AlarmLength;
	struct TON_10msobj Timer_NoFlow;
} FSL_obj;

typedef struct HSCOUNT_obj
{	unsigned char type;
	unsigned long GrossCount;
	unsigned long NetCount;
	unsigned long GrossPulses;
	plcbit Reset;
	unsigned long count_adr;
	plcbit Sim;
	unsigned short FlowFilter;
	float Flow;
	float MF;
	unsigned short well[200];
	float ppg;
	unsigned short count10;
	unsigned long ppg10_adr;
	unsigned long timer_adr;
	struct TOF_10msobj ppg10timer;
	unsigned short count_0;
	unsigned long timer_0;
} HSCOUNT_obj;

typedef struct HWVALVE_obj
{	unsigned long Valve_adr;
	unsigned char ID;
	unsigned char Position;
	unsigned long LSO_adr;
	unsigned long LSC_adr;
	unsigned long LCL_adr;
	struct TOF_10msobj opentimer;
	struct TOF_10msobj closetimer;
	struct TON_10msobj stoptimer;
	struct TON_10msobj traveltimer;
	unsigned long open_cmd_adr;
	unsigned long close_cmd_adr;
	unsigned long stop_cmd_adr;
	plcbit LSO;
	plcbit LSC;
	plcbit LCL;
	plcbit OPNCMD;
	plcbit CLSCMD;
	plcbit STPCMD;
	plcbit _open;
	plcbit _close;
	unsigned char cmd_typ;
} HWVALVE_obj;

typedef struct IQVALVE_obj
{	unsigned char Type;
	plcbit Enable;
	unsigned long Valve_adr;
	unsigned char ID;
	plcbit code_02[24];
	unsigned short code_04[2];
	unsigned short Position;
	unsigned short Percent;
	unsigned short Torque;
	plcbit Comm_Stat[2];
	plcbit Status;
	unsigned short Command;
	plcbit Command_FQ;
	plcbit Cmd_Stat[2];
	unsigned char Alarm[3];
	plcbit bPoll_02[2];
	plcbit bPoll_04[2];
	plcbit cPoll[2];
	unsigned long Polls[2];
	unsigned long NoReply[2];
	unsigned long Reply[2];
	plcbit Retry[2];
	unsigned char ReconnectCount[2];
	unsigned char RemoveCount[2];
	plcbit ClockWiseClose;
	plcbit MessageOK[2];
	plcbit MessageFail[2];
	struct TON_10msobj ScanTimer;
	struct TON_10msobj traveltimer;
	plcbit _open;
	plcbit _close;
} IQVALVE_obj;

typedef struct METER_obj
{	unsigned long security_adr;
	unsigned short NetFlow;
	float Pressure;
	float Gravity;
	unsigned long PreviousNet;
	unsigned long ActiveNet;
	unsigned long ActiveSemiNet;
	unsigned long BatchSequence;
	unsigned long BatchNetA;
	unsigned long BatchNetB;
	unsigned long ResetCounter;
	unsigned long RoomCounter;
	plcbit eob_oip;
	plcbit eob_cmd;
	plcbit reset_oip;
	unsigned long OldNet;
	unsigned long OldSemi;
	unsigned short OldSequence;
	plcbit ActiveA;
	struct TON_10msobj timer;
	unsigned short error_reset;
	unsigned short error_room;
	plcbit ResetOldNet;
	plcbit ResetOldSemi;
	unsigned char type;
	unsigned long net1ppb_adr;
	unsigned long snet1ppb_adr;
	unsigned long batch_sts_adr;
	unsigned long eob_cmd_adr;
	plcbit Net1PPB;
	plcbit SNet1PPB;
	plcbit BatchAStatus;
	plcbit batchAStatus;
	unsigned long newnetbbls;
	plcbit net1ppb;
	plcbit snet1ppb;
} METER_obj;

typedef struct MXVALVE_obj
{	plcbit Enable;
	unsigned long Valve_adr;
	unsigned char ID;
	unsigned short Register[3];
	unsigned short Position;
	unsigned short Percent;
	plcbit Comm_Stat[2];
	plcbit Status;
	unsigned short Command;
	unsigned short Cmd_Stat[2];
	unsigned short Alarm[2];
	plcbit bPoll[2];
	plcbit cPoll[2];
	unsigned long Polls[2];
	unsigned long NoReply[2];
	unsigned long Reply[2];
	plcbit Retry[2];
	unsigned char ReconnectCount[2];
	unsigned char RemoveCount[2];
	plcbit MessageOK[2];
	plcbit MessageFail[2];
	struct TON_10msobj ScanTimer;
} MXVALVE_obj;

typedef struct OMNICMD_obj
{	plcbit Prove[8];
	plcbit Abort;
	plcbit PumpStart;
	plcbit PumpStop;
} OMNICMD_obj;

typedef struct OMNIDATA_obj
{	unsigned short Raw3901[2];
	unsigned long Raw5501[14];
	float Raw7991[30];
	float RunNum;
	float MeterNum;
	float Tempreture[5];
	float Pressure[5];
	float Gravity[5];
	float Flowrate[5];
	float Counts[6];
} OMNIDATA_obj;

typedef struct OMNISTATUS_obj
{	plcbit Raw1909[16];
	plcbit Raw1080[16];
	plcbit Abort_TempU;
	plcbit Abort_TempD;
	plcbit Sequence_Comp;
	plcbit Sequence_Abort;
	plcbit Flight_Det1_Tripped;
	plcbit Flight_Det3_Tripped;
	plcbit OTravel_Det2_Tripped;
	plcbit OTravel_Det4_Tripped;
	plcbit Ball_Forward;
	plcbit Ball_Reverse;
	plcbit Abort_RunD;
	plcbit Abort_Seal_NotOK;
	plcbit Abort_FlowrateU;
	plcbit Abort_No_Permis;
	plcbit Factor_Not_Imple;
	plcbit Abort_Not_Running;
	plcbit Sequence_Progress;
	plcbit Pump_Run;
	plcbit Seal;
	plcbit Low_N2_Pressure;
	plcbit Meter_Active;
	plcbit Proving_Active;
	plcbit Prove_Complete;
	plcbit Prove_Aborted;
} OMNISTATUS_obj;

typedef struct OMNI_obj
{	unsigned short Gravity;
	plcbit CommFail;
	struct OMNISTATUS_obj Status;
	struct OMNICMD_obj Cmd;
	struct OMNIDATA_obj Data;
	struct ALARM_obj StatusGroup[2];
} OMNI_obj;

typedef struct POLL_obj
{	unsigned char NumRetry;
	unsigned char NumValves;
	unsigned long InterPollDelay;
	unsigned char Active_Ch;
	unsigned char Old_Ch;
	signed char Poll_Num;
	plcbit ValveError;
	plcbit ValveChError[2];
	unsigned char Reconnect;
	plcbit CommandReady;
	unsigned char CommandValve;
	signed char MovingScan;
	plcbit MovingScanToggle;
	plcbit MovingTableFull;
	unsigned long Polls[2];
	unsigned long NoReply[2];
	unsigned long Reply[2];
	plcbit Enable_Ch1;
	plcbit Enable_Ch2;
	unsigned long PortOK[2];
	unsigned long StateMachine_adr;
	unsigned long Valve_adr;
	unsigned long ActiveValve_adr;
	unsigned long CommandValve_adr;
	unsigned long MovingValve_adr;
	plcbit Turbo;
} POLL_obj;

typedef struct PORTtyp
{	unsigned short exec;
	unsigned short oldexec;
	unsigned short status;
	unsigned short opstatus;
	unsigned long device;
	unsigned long mode;
	unsigned long ident;
	unsigned char address;
	unsigned short timeout;
	plcbit fail;
	struct TON_10msobj timer;
	unsigned long adra;
} PORTtyp;

typedef struct RCA_READtyp
{	unsigned short StatusGroup1;
	unsigned short StatusGroup2;
	unsigned short StatusGroup3;
	unsigned short AlarmGroup1;
	unsigned short AlarmGroup2;
	unsigned short AlarmGroup3;
	unsigned short AlarmGroup4;
	unsigned short AlarmGroup5;
	unsigned short ShipperAStatus1;
	unsigned short ShipperAStatus2;
	unsigned short ShipperAComm;
	unsigned short ShipperAFault;
	unsigned short ShipperALocal;
	unsigned short ShipperBStatus1;
	unsigned short ShipperBStatus2;
	unsigned short ShipperBComm;
	unsigned short ShipperBFault;
	unsigned short ShipperBLocal;
	unsigned short RecipeAStatus;
	unsigned short RecipeBStatus;
	unsigned short NonShipperStatus1;
	unsigned short NonShipperStatus2;
	unsigned short NonShipperComm;
	unsigned short NonShipperFault;
	unsigned short NonShipperLocal;
	unsigned long CurrentRoomA;
	unsigned long NextRoomA;
	unsigned long SecondRoomA;
	unsigned long CurrentQuotaA;
	unsigned long NextQuotaA;
	unsigned long SecondQuotaA;
	unsigned short CurrentStepA;
	unsigned short NextStepA;
	unsigned short SecondStepA;
	unsigned short QuotaDevA;
	unsigned long CurrentRoomB;
	unsigned long NextRoomB;
	unsigned long SecondRoomB;
	unsigned long CurrentQuotaB;
	unsigned long NextQuotaB;
	unsigned long SecondQuotaB;
	unsigned short CurrentStepB;
	unsigned short NextStepB;
	unsigned short SecondStepB;
	unsigned short QuotaDevB;
	unsigned short Line1IncomingPressure;
	unsigned short Line1TerminalPressure;
	unsigned short Line1MeterTemp;
	unsigned short Line1MeterGravity;
	unsigned short Line1LocalPressureSP;
	unsigned short Line1SelectedPressureSP;
	unsigned short Line1RampedPressureSP;
	unsigned short Line1ControlValve;
	unsigned short Line1NextGravitySP;
	unsigned short Line1SecondGravitySP;
} RCA_READtyp;

typedef struct RCA_WRITEtyp
{	unsigned long CurrentRoomA;
	unsigned long NextRoomA;
	unsigned long SecondRoomA;
	unsigned long CurrentQuotaA;
	unsigned long NextQuotaA;
	unsigned long SecondQuotaA;
	unsigned short CurrentStepA;
	unsigned short NextStepA;
	unsigned short SecondStepA;
	unsigned short QuotaDevA;
	unsigned long CurrentRoomB;
	unsigned long NextRoomB;
	unsigned long SecondRoomB;
	unsigned long CurrentQuotaB;
	unsigned long NextQuotaB;
	unsigned long SecondQuotaB;
	unsigned short CurrentStepB;
	unsigned short NextStepB;
	unsigned short SecondStepB;
	unsigned short QuotaDevB;
	unsigned short B1_BV_Open;
	unsigned short B1_BV_Close;
	unsigned short B1_SV_Open;
	unsigned short B1_SV_Close;
	unsigned short B1_Start;
	unsigned short B1_Stop;
	unsigned short MTR_Open;
	unsigned short MTR_Close;
	unsigned short MAP_Open;
	unsigned short MAP_Close;
	unsigned short TerminalCrash;
	unsigned short Prove_Start;
	unsigned short Prove_Abort;
	unsigned short Satellite_Pulse;
	unsigned short Modem_Reset;
	unsigned short MasterArmA;
	unsigned short MasterUnarmA;
	unsigned short RemoteTriggerA;
	unsigned short TicketTriggerA;
	unsigned short NextQuotaArmA;
	unsigned short NextQuotaUnarmA;
	unsigned short NextGravArmA;
	unsigned short NextGravUnarmA;
	unsigned short NextColorArmA;
	unsigned short NextColorUnarmA;
	unsigned short NextTickArmA;
	unsigned short NextTickUnarmA;
	unsigned short SecondQuotaArmA;
	unsigned short SecondQuotaUnarmA;
	unsigned short SecondGravArmA;
	unsigned short SecondGravUnarmA;
	unsigned short SecondColorArmA;
	unsigned short SecondColorUnarmA;
	unsigned short SecondTickArmA;
	unsigned short SecondTickUnarmA;
	unsigned short MasterArmB;
	unsigned short MasterUnarmB;
	unsigned short RemoteTriggerB;
	unsigned short TicketTriggerB;
	unsigned short NextQuotaArmB;
	unsigned short NextQuotaUnarmB;
	unsigned short NextGravArmB;
	unsigned short NextGravUnarmB;
	unsigned short NextColorArmB;
	unsigned short NextColorUnarmB;
	unsigned short NextTickArmB;
	unsigned short NextTickUnarmB;
	unsigned short SecondQuotaArmB;
	unsigned short SecondQuotaUnarmB;
	unsigned short SecondGravArmB;
	unsigned short SecondGravUnarmB;
	unsigned short SecondColorArmB;
	unsigned short SecondColorUnarmB;
	unsigned short SecondTickArmB;
	unsigned short SecondTickUnarmB;
	unsigned short PDMV1_Open;
	unsigned short PDMV1_Close;
	unsigned short PDMV2_Open;
	unsigned short PDMV2_Close;
	unsigned short PDMV3_Open;
	unsigned short PDMV3_Close;
	unsigned short PDMV4_Open;
	unsigned short PDMV4_Close;
	unsigned short PDMV5_Open;
	unsigned short PDMV5_Close;
	unsigned short MLPressureSP;
	unsigned short TLPressureSP;
	unsigned short NextGravityA;
	unsigned short SecondGravityA;
	unsigned short NextGravityB;
	unsigned short SecondGravityB;
	unsigned char Time[8];
} RCA_WRITEtyp;

typedef struct RECIPECMDS_obj
{	plcbit MasterArm;
	plcbit MasterUnarm;
	plcbit master_oip;
	plcbit RemoteTrigger;
	plcbit TicketTrigger;
	plcbit NextQuotaArm;
	plcbit NextQuotaUnarm;
	plcbit SecondQuotaArm;
	plcbit SecondQuotaUnarm;
	plcbit ThirdQuotaArm;
	plcbit ThirdQuotaUnarm;
	plcbit FourthQuotaArm;
	plcbit FourthQuotaUnarm;
	plcbit NextGravArm;
	plcbit NextGravUnarm;
	plcbit SecondGravArm;
	plcbit SecondGravUnarm;
	plcbit ThirdGravArm;
	plcbit ThirdGravUnarm;
	plcbit FourthGravArm;
	plcbit FourthGravUnarm;
	plcbit NextColorArm;
	plcbit NextColorUnarm;
	plcbit SecondColorArm;
	plcbit SecondColorUnarm;
	plcbit ThirdColorArm;
	plcbit ThirdColorUnarm;
	plcbit FourthColorArm;
	plcbit FourthColorUnarm;
	plcbit NextTickArm;
	plcbit NextTickUnarm;
	plcbit SecondTickArm;
	plcbit SecondTickUnarm;
	plcbit ThirdTickArm;
	plcbit ThirdTickUnarm;
	plcbit FourthTickArm;
	plcbit FourthTickUnarm;
	plcbit GtyWindowOpenArm;
	plcbit GtyWindowOpenUnarm;
} RECIPECMDS_obj;

typedef struct STEP_obj
{	unsigned short Step;
	unsigned short ShipNum;
	plcbit QuotaEnable;
	plcbit GravityEnable;
	plcbit ColorEnable;
	plcbit TicketEnable;
	struct ANALOG_obj GravSP;
	unsigned long RoomCount;
	unsigned long Quota;
	plcbit GravityWindowOpen;
	unsigned short Step_Raw;
	unsigned short Step_Raw_Out;
	unsigned long Step32_Raw;
	unsigned long Step32_Raw_out;
	unsigned long Step32;
	unsigned long Step64[2];
	unsigned long Step64_Raw[2];
} STEP_obj;

typedef struct RECIPE_obj
{	struct STEP_obj current;
	struct STEP_obj next;
	struct STEP_obj second;
	struct STEP_obj third;
	struct STEP_obj fourth;
	struct RECIPECMDS_obj command;
	unsigned long security_adr;
	unsigned long terminal_adr;
	unsigned long vg_adr;
	unsigned long meter_adr;
	unsigned long gravity_adr;
	unsigned long divider_adr;
	unsigned char Type;
	plcbit Enable;
	plcbit close;
	plcbit crash;
	plcbit LSO;
	float Gravity;
	plcbit GravityRise;
	plcbit GravityFall;
	plcbit GravityChange;
	plcbit GravityMiss;
	struct TON_10msobj GravityTimer;
	plcbit QuotaWindow;
	unsigned short QuotaDev;
	unsigned long QuotaDevLow;
	unsigned long QuotaDevHigh;
	unsigned long DisplaceVolume;
	unsigned short shipper_oip;
	unsigned short shipper_rca;
	unsigned long shipper_rca32;
	unsigned long shipper_rca64[2];
	plcbit NewShipCmd;
	plcbit Trigger;
	plcbit TriggerOnQuota;
	plcbit TriggerOnGravity;
	plcbit TriggerOnColor;
	plcbit TriggerAlarm;
	struct TOF_10msobj TriggerTimer;
	unsigned short TriggerPacked;
	unsigned short TriggerPacked2;
	plcbit OIP_Trigger;
	plcbit Event;
	plcbit AnyInTravel;
	plcbit InTravel;
	plcbit FullOpen;
	plcbit OverRunBypass;
	plcbit OverRun;
	plcbit OverRunEnable;
	struct TON_10msobj OverRunTimer;
	plcbit OverRunArmed;
	plcbit CloseAll;
	struct TON_10msobj DelayCloseTimer;
	plcbit BatchChangeArmed;
	unsigned long RoomWarningBbls;
	plcbit RoomWarning;
	plcbit RoomExpired;
	plcbit room_oip;
	plcbit room_enb;
	plcbit MasterTrigger;
	plcbit TicketTrigger;
	plcbit ManualTrigger;
	unsigned short status;
	struct TON_10msobj RepulseCloseTimer;
	plcbit _oldQuotaWindow;
	plcbit MainlineRecipe;
	plcbit GravityTrigOnly;
	struct TON_10msobj GravityTrigTimer;
} RECIPE_obj;

typedef struct RECORDER_obj
{	plcbit PortOK;
	plcbit CommFail;
	plcbit GoodMsg;
	unsigned short LastBadStatus;
	plcbit SystemError;
	plcbit DiskAlmostFull;
	unsigned char num_ch;
	struct TON_10msobj timer;
	unsigned short channels[10];
	unsigned long ch_adr[10];
	unsigned short read[4];
} RECORDER_obj;

typedef struct RMVALVE_obj
{	unsigned long v_adr;
	struct TON_10msobj _timer;
} RMVALVE_obj;

typedef struct RUNTIME_obj
{	unsigned long run_adr;
	plcbit run;
	unsigned long tick_adr;
	plcbit tick;
	plcbit Reset_oip;
	unsigned short Minutes;
	unsigned short Hours;
	plcstring TimeString[21];
	float _Hours;
	plcbit _old;
} RUNTIME_obj;

typedef struct SCALE_obj
{	unsigned long ptrRawValue;
	float RawMax;
	float RawMin;
	float ScaledMax;
	float ScaledMin;
	float ScaledValue;
	signed short value_i;
	unsigned short value_64k;
	unsigned char Error;
	unsigned short initStatus;
	float RawValue;
	unsigned short _valueUINT;
	signed short _valueINT;
	float _valueFLOAT;
	unsigned char typ;
} SCALE_obj;

typedef struct SECURITY_obj
{	unsigned short Passcode;
	plcbit Intruder;
	plcbit IntruderBypass;
	plcbit Level0;
	plcbit Level1;
	plcbit Level2;
	plcbit IntruderAlarm;
	unsigned char access_led;
	struct TON_10msobj EnterTimer;
	struct TON_10msobj ExitTimer;
	unsigned long intruder_adr;
	unsigned long bypass_adr;
	unsigned short Passcode_L1;
	unsigned short Passcode_L2;
	plcbit ExitTime;
	plcbit EnterTime;
	plcbit FirstEntry;
} SECURITY_obj;

typedef struct SMTIMER
{	signed short time;
	signed short elapse;
	unsigned char done;
	unsigned char donepulse;
} SMTIMER;

typedef struct STATEMACHINE
{	signed short state;
	unsigned char state1Scan;
	signed short statenext;
	signed short statereturn;
	struct TON_10msobj t10[4];
	struct SMTIMER t[4];
	unsigned char description[64];
} STATEMACHINE;

typedef struct SUMP_obj
{	plcstring Message[21];
	plcstring Name[21];
	plcbit start_oip;
	plcbit start_remote;
	plcbit stop_oip;
	plcbit stop_remote;
	plcbit toggle_oip;
	plcbit on_led;
	plcbit off_led;
	plcbit motor_run_cmd;
	plcbit Auto;
	plcbit LL;
	plcbit HL;
	plcbit HHL;
	plcbit FS;
	float HL_Sp;
	float HHL_Sp;
	float LL_Sp;
	plcbit AlarmHL;
	plcbit AlarmStartFailure;
	plcbit AlarmNoFlow;
	unsigned short h_alarm_secs;
	unsigned short fail_alarm_secs;
	unsigned short FlowDelaySecs;
	plcbit shutof_valve_cmd;
	unsigned long security_adr;
	unsigned long terminal_adr;
	unsigned long analog_adr;
	unsigned long secs_adr;
	float level;
	plcbit motor_run;
	plcbit shutof_valve;
	plcbit _motor_run_cmd;
	unsigned short _t0;
	unsigned short _t1;
	plcbit _hl;
} SUMP_obj;

typedef struct TERMINAL_obj
{	unsigned long security_adr;
	unsigned long mlpressure_adr;
	unsigned short MLPressure;
	plcbit crash;
	plcbit crash_oip;
	plcbit crash_remote;
	plcbit crash_reset;
	plcbit crash_repulse;
	plcbit lockout;
	plcbit lockout_reset_oip;
	plcbit shutdown;
	plcbit ControlPowerFail;
	plcbit close;
	plcbit CloseDel;
	unsigned short message;
	plcbit remote;
	plcbit local_oip;
	unsigned char local_led;
	plcbit remote_oip;
	unsigned char remote_led;
	plcbit ProverBypass;
	unsigned short TotalVolume;
	unsigned short TerminalVolume;
	unsigned short ProverVolume;
	plcbit GasBypass;
	plcbit FireBypass;
	plcbit SmokeBypass;
	plcbit FireFault;
	plcbit HazGasFault;
	plcbit FireAlarm;
	plcbit HazGasHiAlarm;
	plcbit HazGasHiHiAlarm;
	plcbit SmokeAlarm;
	plcbit TransmitterFail;
	plcbit LowSucPrs;
	plcbit HiDischgPrs;
} TERMINAL_obj;

typedef struct TIMER_obj
{	unsigned short C10ms;
	unsigned short C100ms;
	unsigned short C2s;
	unsigned short C1s;
	unsigned short C10s;
	unsigned short Cxs;
	plcbit F100ms;
	plcbit F2s;
	plcbit F1s;
	plcbit F10s;
	unsigned char x;
	unsigned short old;
} TIMER_obj;

typedef struct VALVEGROUP_obj
{	unsigned char number;
	unsigned long valve_adr;
	plcbit type;
	unsigned short status[2];
	unsigned short comm;
	plcbit newcomm;
	unsigned short oldcomm;
	unsigned short fault;
	plcbit newfault;
	unsigned short oldfault;
	unsigned short local;
	unsigned short failed;
	plcbit newfailed;
	unsigned short oldfailed;
	unsigned short commAckImg;
	plcbit commAckImg_PP41[16];
	unsigned short faultAckImg;
	plcbit faultAckImg_PP41[16];
	unsigned short localAckImg;
	plcbit localAckImg_PP41[16];
	unsigned short failedAckImg;
	plcbit failedAckImg_PP41[16];
	plcbit commUnAckAlm;
	plcbit faultUnAckAlm;
	plcbit localUnAckAlm;
	plcbit failedUnAckAlm;
	plcbit horn;
	plcbit AllClosed;
	plcbit OneOpened;
	plcbit FaultGroup[16];
	plcbit LocalGroup[16];
	plcbit CommfailGroup[16];
	plcbit StatusGroup[32];
	plcbit FailedGroup[16];
} VALVEGROUP_obj;

typedef struct AND4
{
	/* VAR_INPUT (digital) */
	plcbit input1;
	plcbit input2;
	plcbit input3;
	plcbit input4;
	/* VAR_OUTPUT (digital) */
	plcbit output;
} AND4_typ;

typedef struct RTUV
{
	/* VAR_INPUT (digital) */
	plcbit cmd_in;
	plcbit stop_in;
	plcbit first;
	/* VAR_OUTPUT (digital) */
	plcbit stop;
	plcbit cmd;
	plcbit active;
	/* VAR (digital) */
	plcbit stop_old;
} RTUV_typ;

typedef struct LD_UINT
{
	/* VAR_INPUT (analog) */
	unsigned short IN1;
	/* VAR_OUTPUT (analog) */
	unsigned short OUT;
	/* VAR_INPUT (digital) */
	plcbit EN;
} LD_UINT_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void AND4(struct AND4* inst);
_BUR_PUBLIC void RTUV(struct RTUV* inst);
_BUR_PUBLIC void LD_UINT(struct LD_UINT* inst);
_BUR_PUBLIC signed long scale_serv(struct SCALE_obj* scale);
_BUR_PUBLIC unsigned short hwv_serv(struct HWVALVE_obj* HWValve, unsigned short number);
_BUR_PUBLIC plcbit analog_serv(struct ANALOG_obj* anal);
_BUR_PUBLIC plcbit alarm_init(struct ALARM_obj* alarm, unsigned short horn_mask, unsigned short crash_mask, unsigned long duration);
_BUR_PUBLIC unsigned short alarm_serv(struct ALARM_obj* alarm);
_BUR_PUBLIC plcbit record_init(struct RECORDER_obj* record, unsigned char num_ch, unsigned long c1, unsigned long c2, unsigned long c3, unsigned long c4, unsigned long c5, unsigned long c6, unsigned long c7, unsigned long c8, unsigned long c9, unsigned long c10, unsigned long duration);
_BUR_PUBLIC plcbit record_serv(struct RECORDER_obj* record);
_BUR_PUBLIC plcbit vgroup_serv(struct VALVEGROUP_obj* vg);
_BUR_PUBLIC plcbit clock_init(struct CLOCK_obj* clock, unsigned long rcatime_adr, unsigned long duration);
_BUR_PUBLIC plcbit mx_init(struct POLL_obj* Poll, unsigned long Valve_adr, unsigned long SM_adr, unsigned char NumValves, unsigned char NumRetry, unsigned char Reconnect, float InterPollDelay, unsigned long PortOKCh1_adr, unsigned long PortOKCh2_adr);
_BUR_PUBLIC unsigned short mx_serv(struct POLL_obj* Poll);
_BUR_PUBLIC plcbit mx_command(struct POLL_obj* Poll, unsigned char Valve, unsigned short Command);
_BUR_PUBLIC plcbit mxv_init(struct MXVALVE_obj* m, unsigned long Valve_adr, plcbit Enable, unsigned long ScanTime, unsigned char ID);
_BUR_PUBLIC plcbit hwv_init(struct HWVALVE_obj* HWValve, unsigned long Valve_adr, unsigned long LSO_adr, unsigned long LSC_adr, unsigned long LCL_adr, unsigned long open_cmd_adr, unsigned long close_cmd_adr, unsigned long stop_cmd_adr, unsigned long cmdduration, unsigned long stopduration, unsigned long travelduration, unsigned char cmd_typ);
_BUR_PUBLIC plcbit floop_serv(struct FL_obj* fl);
_BUR_PUBLIC plcbit pump_init(struct PUMP_obj* p, unsigned long status_adr, unsigned long run_adr, unsigned long pb_pt);
_BUR_PUBLIC plcbit pump_serv(struct PUMP_obj* p, plcbit Auto, plcbit Hand, plcbit Overload);
_BUR_PUBLIC plcbit BitToAdr(unsigned long bit_adr, unsigned long byte_adr, unsigned char l);
_BUR_PUBLIC plcbit RunTime(struct RUNTIME_obj* runtime);
_BUR_PUBLIC plcbit term_serv(struct TERMINAL_obj* terminal);
_BUR_PUBLIC plcbit booster_init(struct BOOSTER_obj* b, unsigned long StartupOverride, unsigned long StartDelay, unsigned long LowFlow, unsigned short horn_mask, unsigned short crash_mask, unsigned long duration);
_BUR_PUBLIC unsigned short iq2_serv(struct POLL_obj* Poll);
_BUR_PUBLIC plcbit dsump_init(struct DSUMP_obj* dsump, plcstring* name, unsigned long terminal_adr, unsigned long security_adr, unsigned long secs_adr, unsigned short FlowDelaySecs);
_BUR_PUBLIC plcbit dsump_serv(struct DSUMP_obj* dsump);
_BUR_PUBLIC unsigned short det_serv(struct DETECTOR_obj* det);
_BUR_PUBLIC plcbit det_init(struct DETECTOR_obj* det, unsigned long raw_adr, unsigned long filter_pt, unsigned long faultalarm_pt, unsigned long firealarm_pt);
_BUR_PUBLIC plcbit RunTime_init(struct RUNTIME_obj* runtime, unsigned long run_adr, unsigned long tick_adr);
_BUR_PUBLIC plcbit rmv_serv(struct RMVALVE_obj* r);
_BUR_PUBLIC plcbit rmv_init(struct RMVALVE_obj* r, unsigned long v_adr, plctime timerPT);
_BUR_PUBLIC plcbit hscount_serv(struct HSCOUNT_obj* h);
_BUR_PUBLIC plcbit hscount_init(struct HSCOUNT_obj* h, unsigned long count_adr, float ppg, unsigned long timer_adr, float Flow_Type, unsigned char type, unsigned long ppg10_adr);
_BUR_PUBLIC signed long scale_init(struct SCALE_obj* scale, float rawMin, float rawMax, float scaledMin, float scaledMax, unsigned long adrRawValue, unsigned char typ);
_BUR_PUBLIC plcbit sump_init(struct SUMP_obj* sump, plcstring* name, unsigned long terminal_adr, unsigned long security_adr, unsigned long analog_adr, unsigned long secs_adr, float h_level, float hh_level, float l_level, unsigned short hh_alarm_secs, unsigned short fail_alarm_secs, unsigned short FlowDelaySecs);
_BUR_PUBLIC plcbit iqv_init(struct IQVALVE_obj* IQValve, unsigned long Valve_adr, plcbit Enable, unsigned long ScanTime, unsigned char ID, plcbit ClockWiseClose, unsigned char Type);
_BUR_PUBLIC plcbit omni_serv(struct OMNI_obj* o);
_BUR_PUBLIC plcbit omni_init(struct OMNI_obj* o, unsigned long horn_mask, unsigned long crash_mask, unsigned long duration);
_BUR_PUBLIC plcbit sump_serv(struct SUMP_obj* sump);
_BUR_PUBLIC plcbit timer_serv(struct TIMER_obj* timer);
_BUR_PUBLIC plcbit vgroup_init(struct VALVEGROUP_obj* vg, unsigned long valve_adr, unsigned char number, plcbit type);
_BUR_PUBLIC plcbit secur_init(struct SECURITY_obj* s, unsigned long intruder_adr, unsigned long bypass_adr, unsigned short pw1, unsigned short pw2, unsigned long duration);
_BUR_PUBLIC plcbit secur_serv(struct SECURITY_obj* s);
_BUR_PUBLIC plcbit analog_init(struct ANALOG_obj* anal, unsigned long raw_adr, float high, float low, unsigned char type, unsigned short mult);
_BUR_PUBLIC unsigned short BitToWord(unsigned long bit_adr);
_BUR_PUBLIC plcbit WtoB(unsigned short w, unsigned long bit_adr);
_BUR_PUBLIC plcbit floop_init(struct FL_obj* fl, unsigned long psi_pt);
_BUR_PUBLIC plcbit booster_serv(struct BOOSTER_obj* b);
_BUR_PUBLIC plcbit clock_serv(struct CLOCK_obj* clock);
_BUR_PUBLIC plcbit term_init(struct TERMINAL_obj* terminal, unsigned long security_adr, unsigned long mlpressure_adr, unsigned short terminalvolume, unsigned short provervolume);
_BUR_PUBLIC plcbit meter_serv(struct METER_obj* meter);
_BUR_PUBLIC plcbit meter_init(struct METER_obj* meter, unsigned long security_adr, unsigned long net1ppb_adr, unsigned long snet1ppb_adr, unsigned long batch_sts_adr, unsigned long eob_cmd_adr, unsigned char type);
_BUR_PUBLIC plcbit recipe_serv(struct RECIPE_obj* recipe);
_BUR_PUBLIC plcbit recipe_init(struct RECIPE_obj* recipe, unsigned long security_adr, unsigned long terminal_adr, unsigned long vg_adr, unsigned long meter_adr, unsigned long gravity_adr, unsigned long closedelay, unsigned long RoomWarningBbls, unsigned long OverRunPT, unsigned long repulsePT, unsigned long divider_adr, unsigned char type);
_BUR_PUBLIC unsigned short iq_serv(struct POLL_obj* Poll);
_BUR_PUBLIC plcbit FSL(struct FSL_obj* fsl, plcbit MasterStart, plcbit Fsl, unsigned long StartOverrideDly, unsigned long AlarmLengthDly, unsigned long NoFlowDly);


#ifdef __cplusplus
};
#endif
#endif /* _BPL_LIB_ */

