/* Automation Studio generated header file */
/* Do not edit ! */
/* uMB 2.04.1 */

#ifndef _UMB_
#define _UMB_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _uMB_VERSION
#define _uMB_VERSION 2.04.1
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "sys_lib.h"
		#include "DRV_mbus.h"
#endif
#ifdef _SG3
		#include "sys_lib.h"
		#include "DRV_mbus.h"
#endif
#ifdef _SGC
		#include "sys_lib.h"
		#include "DRV_mbus.h"
#endif

/* Constants */
#ifdef _REPLACE_CONST
 #define CONSECUTIVE 0U
 #define FLOW_IN 1U
 #define FLOW_OUT 2U
 #define IDLE 3U
 #define NON_CONSECUTIVE 1U
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST unsigned char CONSECUTIVE;
 _GLOBAL_CONST unsigned char FLOW_IN;
 _GLOBAL_CONST unsigned char FLOW_OUT;
 _GLOBAL_CONST unsigned char IDLE;
 _GLOBAL_CONST unsigned char NON_CONSECUTIVE;
#endif




/* Datatypes and datatypes of function blocks */
typedef struct ENRAFMB_obj
{	unsigned char State;
	unsigned char NumTanks;
	unsigned short Status;
	unsigned char _ptr;
	unsigned long _umb_adr;
	unsigned long _enraftank_adr;
} ENRAFMB_obj;

typedef struct ENRAFTANKXS_obj
{	float Level;
	float Temperature;
	float FlowRate;
	float TotalObservedVolume;
	float UsableVolume;
	unsigned short AlarmStatus;
	unsigned short AlarmStatus2;
	float Reserved1;
	float Reserved2;
	float GrossObservedVolume;
	float GrossStandardVolume;
	float ObservedDensity;
	float VCF;
	float GaugeHWAlarms;
	float LevelStatus;
	float TempStatus;
	unsigned short Level_16th;
	unsigned short FlowRate_i;
	unsigned char FlowStatus;
	unsigned char Activity[5];
	plcbit bPoll;
	plcbit CommStatus;
	plcbit CommFail;
	plcbit CommFailAlarm;
	plcbit AlarmEnrf[32];
	plcbit LSHH;
	plcbit LSH;
	plcbit LSLL;
	plcbit LSL;
	plcbit HighHighLevelAlarm;
	plcbit HighLevelAlarm;
	plcbit LowLowLevelAlarm;
	plcbit LowLevelAlarm;
	plcbit LevelAlarm;
	plcbit LevelGaugeAlarm;
	plcbit TempGaugeAlarm;
	plcbit GaugeHardwareAlarm;
	plcbit Disable;
	plcstring LevelMessage[61];
	plcstring TempMessage[61];
	plcstring GaugeMessage[61];
	float SafeFill;
	float Room;
	float Gravity;
	unsigned short Temperature_i;
} ENRAFTANKXS_obj;

typedef struct ENRAFTANK_obj
{	float Level;
	float Temperature;
	float FlowRate;
	float TotalObservedVolume;
	float UsableVolume;
	float MaxOperatingLevel;
	float LevelHiHiSP;
	float LevelHiSP;
	float LevelLowSP;
	float GrossStandardVolume;
	float NetStdVolume;
	float ObservedDensity;
	float VCF;
	unsigned short AlarmStatus;
	unsigned short LevelStatus;
	unsigned short TempStatus;
	unsigned short Level_16th;
	unsigned short FlowRate_i;
	unsigned char Status;
	unsigned char Activity[5];
	plcbit bPoll;
	plcbit CommStatus;
	plcbit CommFail;
	plcbit CommFailAlarm;
	plcbit Alarm[16];
	plcbit AlarmEnrf[16];
	plcbit AlarmDI[16];
	plcbit LevelGaugeAlarm;
	plcbit TempGaugeAlarm;
	plcbit LevelAlarm;
	plcbit Disable;
	plcstring LevelMessage[41];
	plcstring TempMessage[41];
	float SafeFill;
	float Room;
	float Gravity;
	unsigned short Temperature_i;
} ENRAFTANK_obj;

typedef struct LNJMB_obj
{	unsigned char State;
	unsigned char NumTanks;
	unsigned short Status;
	unsigned char ReadType;
	unsigned char _tank_ptr;
	unsigned long _umb_adr;
	unsigned long _tank_adr;
	unsigned char _rec_ptr;
} LNJMB_obj;

typedef struct LNJTANK_obj
{	unsigned short GaugeInfo;
	unsigned short Level_32nd;
	signed short Temperature;
	signed short APIGravity;
	unsigned long GrossVolume;
	unsigned long NetVolume;
	unsigned short AlarmStatus;
	signed short FlowRate;
	unsigned short Level_16th;
	unsigned char Status;
	plcbit bPoll[2];
	plcbit CommStatus;
	plcbit CommFail;
	plcbit CommFailAlarm;
	plcbit Alarm[16];
	plcbit AlarmLNJ[16];
	plcbit AlarmDI[16];
	plcbit LevelGaugeCommFail;
	plcbit TempGaugeCommFail;
	plcbit Disable;
} LNJTANK_obj;

typedef struct uMBTime_obj
{	unsigned short Count;
	unsigned long _ticks;
} uMBTime_obj;

typedef struct uMB_obj
{	unsigned long master_adr;
	unsigned long open_adr;
	unsigned short LastBadStatus;
	plcbit PortOK;
	unsigned short LastGoodTime;
	unsigned long RecordNum;
	unsigned long GoodMsg;
	unsigned long BadMsg;
	unsigned short nocomm_sp1;
	unsigned short nocomm_sp2;
	plcbit CommFail;
	plcbit CommFail_Crash;
	plcstring Message[66];
	signed char RecordStatus[200];
	unsigned short ErrorNumber;
	unsigned short _exec;
	plcbit _error;
} uMB_obj;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC signed long LNJ_serv(struct LNJMB_obj* l);
_BUR_PUBLIC signed long Enraf_init(struct ENRAFMB_obj* e, unsigned char NumTanks, unsigned long _enraftank_adr, struct uMB_obj* mb);
_BUR_PUBLIC unsigned short uMBS_init(struct uMB_obj* uMB, struct MBSOpen* pOpen, struct MBSlave* pServ, plcstring* device, plcstring* mode, unsigned char own_ID, plctime timeout, plctime nocomm_sp1, plctime nocomm_sp2);
_BUR_PUBLIC unsigned short uMB_c10ms(struct uMBTime_obj* t);
_BUR_PUBLIC signed long Enraf_serv(struct ENRAFMB_obj* e);
_BUR_PUBLIC unsigned short uMBM_serv(struct uMB_obj* uMB, unsigned short c10ms);
_BUR_PUBLIC unsigned short uMBS_serv(struct uMB_obj* uMB, unsigned short c10ms);
_BUR_PUBLIC unsigned short uMBM_init(struct uMB_obj* uMB, struct MBMOpen* pOpen, struct MBMaster* pServ, plcstring* device, plcstring* mode, plcstring* config, plctime timeout, plctime nocomm_sp1, plctime nocomm_sp2);
_BUR_PUBLIC signed long LNJ_init(struct LNJMB_obj* l, unsigned char NumTanks, unsigned long _tank_adr, struct uMB_obj* mb, unsigned char ReadTyp);
_BUR_PUBLIC signed long EnrafXS_serv(struct ENRAFMB_obj* e);


#ifdef __cplusplus
};
#endif
#endif /* _UMB_ */

