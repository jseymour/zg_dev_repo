/* Automation Studio generated header file */
/* Do not edit ! */
/* ModDiag 2.05.0 */

#ifndef _MODDIAG_
#define _MODDIAG_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _ModDiag_VERSION
#define _ModDiag_VERSION 2.05.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "sys_lib.h"
		#include "DataObj.h"
		#include "standard.h"
		#include "AsIO.h"
		#include "AsIODiag.h"
		#include "AsIOMMan.h"
#endif
#ifdef _SG3
		#include "sys_lib.h"
		#include "DataObj.h"
		#include "standard.h"
		#include "AsIO.h"
		#include "AsIODiag.h"
		#include "AsIOMMan.h"
#endif
#ifdef _SGC
		#include "sys_lib.h"
		#include "DataObj.h"
		#include "standard.h"
		#include "AsIO.h"
		#include "AsIODiag.h"
		#include "AsIOMMan.h"
#endif

/* Constants */
#ifdef _REPLACE_CONST
 #define MAX_MODULE_NUMBER 600U
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST unsigned short MAX_MODULE_NUMBER;
#endif




/* Datatypes and datatypes of function blocks */
typedef struct IODPList_typ
{	unsigned long StartAdrBuffer;
	unsigned short NumberDigitalInput;
	unsigned short NumberDigitalOutput;
	unsigned short NumberAnalogInput;
	unsigned short NumberAnalogOutput;
	unsigned short TotalIONumber;
} IODPList_typ;

typedef struct SingleIOEntry_typ
{	plcstring ModuleName[21];
	plcstring Device[61];
	plcstring ID[61];
	plcstring LogicalName[41];
	unsigned char Type;
	plcstring PVName[513];
} SingleIOEntry_typ;

typedef struct SingleIOStatus_typ
{	plcbit ForceActive;
	double ActForceValue;
	double ActValue;
	unsigned char DataType;
} SingleIOStatus_typ;

typedef struct ModuleAlarmInformation_typ
{	plcstring ErrorModulePath[31];
	plcstring ErrorModuleName[21];
	plcbit AlarmBitField[1800];
	plcbit ACKBitField[1800];
} ModuleAlarmInformation_typ;

typedef struct SingleModuleInformationData_typ
{	plcstring ModulePath[31];
	plcstring ModuleName[21];
	unsigned char ModuleState;
	unsigned char ModuleColor;
	plcstring ErrorModuleName[21];
} SingleModuleInformationData_typ;

typedef struct ModuleInformationBuffer_typ
{	struct SingleModuleInformationData_typ ModuleInformation[600];
} ModuleInformationBuffer_typ;

typedef struct ModuleDiagnose
{
	/* VAR_INPUT (analog) */
	unsigned long AdrModuleInformationBuffer;
	unsigned char Option;
	unsigned long AdrAlarmInformation;
	/* VAR_OUTPUT (analog) */
	unsigned short status;
	/* VAR (analog) */
	struct SingleModuleInformationData_typ ActualModuleInformation;
	plcstring PathBuffer[600][31];
	unsigned char Step;
	unsigned char LastStep;
	unsigned short NumberOfTotalModules;
	unsigned short ModuleIndex;
	unsigned short ModuleNumber;
	unsigned short ModulesLastCheck;
	unsigned short NumberNotCheckedModules;
	unsigned short ModuleDifference;
	unsigned short ResetModuleAlarms;
	struct TON WaitTimer;
} ModuleDiagnose_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void ModuleDiagnose(struct ModuleDiagnose* inst);
_BUR_PUBLIC unsigned short IOStatus(unsigned long pIOList, unsigned long pStatusBuffer, unsigned short StartElements, unsigned short EndElements, unsigned long pEnableForce, unsigned long pForceValue, unsigned long pAnyIOForced);
_BUR_PUBLIC unsigned short IOList(unsigned long pIOInformation);


#ifdef __cplusplus
};
#endif
#endif /* _MODDIAG_ */

