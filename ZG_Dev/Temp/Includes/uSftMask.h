/* Automation Studio generated header file */
/* Do not edit ! */
/* uSftMask  */

#ifndef _USFTMASK_
#define _USFTMASK_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC unsigned short Switch(unsigned short myinput);
_BUR_PUBLIC unsigned short Complement(unsigned short myinput);
_BUR_PUBLIC unsigned char MaskAndShiftI(unsigned short myinput, unsigned char mysize, unsigned char myshift);
_BUR_PUBLIC unsigned char InvMaskShiftB(unsigned short myinput, unsigned char mysize, unsigned char myshift);
_BUR_PUBLIC unsigned char MaskAndShiftB(unsigned short myinput, unsigned char mysize, unsigned char myshift);
_BUR_PUBLIC unsigned short MaskAndShift(unsigned short myinput, unsigned char mysize, unsigned char myshift);
_BUR_PUBLIC unsigned short InvSwitch(unsigned short myinput);


#ifdef __cplusplus
};
#endif
#endif /* _USFTMASK_ */

